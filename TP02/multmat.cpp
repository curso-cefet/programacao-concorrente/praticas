#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <iostream>
#include <chrono>

using namespace std;

typedef struct {
    int idThread;
    float **matA;
    float **matB;
    float **matRes;
    int qtLinhasA;
    int qtLinhasB;
    int qtColunasA;
    int qtColunasB;
    int colunaMin;
    int colunaMax;
} tv_Args;

void *calculaColunaThread(void *arg);

void calculaColuna (
    int coluna, 
    float **matA,
    float **matB, 
    float **matRes, 
    int qtLinhasA,
    int qtColunasA,
    int qtColunasB
);

tv_Args* getArgs(
    int idThread, 
    float **matA,
    float **matB, 
    float **matRes, 
    int qtLinhasA,
    int qtColunasA,
    int qtColunasB,
    int sizePerThread
);

int main(int argc, char *argv[] ) {
    if(argc < 5) {
        cout << "--ERRO: Argumentos inválidos\n\n";
        return -1;
    }

    string file_entrada_a_name = argv[1];
    string file_entrada_b_name = argv[2];
    string file_saida_name = argv[3];
    int num_threads = atoi(argv[4]);

    FILE *fentrada1, *fentrada2, *fsaida;

    fentrada1 = fopen(file_entrada_a_name.c_str(), "r");
    fentrada2 = fopen(file_entrada_b_name.c_str(), "r");
    fsaida = fopen(file_saida_name.c_str(), "w");

    if(fentrada1 == NULL) perror ("Falha ao abrir arquivo de entrada A");
    if(fentrada2 == NULL) perror ("Falha ao abrir arquivo de entrada B");
    if(fsaida == NULL) perror ("Falha ao abrir arquivo de saída");

    clock_t start = clock();

    double tempo_total = 0;
    double tempo_processamento = 0;

    int qtLinhasA, qtLinhasB;
    int qtColunasA, qtColunasB;

    float **matA, **matB, **matRes;

    fscanf(fentrada1, "%d %d", &qtLinhasA, &qtColunasA);
    fscanf(fentrada2, "%d %d", &qtLinhasB, &qtColunasB);

    if(qtColunasA != qtLinhasB) {
        puts("\n\nNúmero de colunas da primeira matriz deve ser igual ao número de linhas da segunda");
        return -1;
    }

    matA = (float**) malloc(sizeof(float*) * qtLinhasA);
    if(matA == NULL)
        return -1;
    
    for(int i = 0; i < qtLinhasA; i++) {
        matA[i] = (float*) malloc(sizeof(float) * qtColunasA);
        if(matA[i] == NULL)
            return -1;

        for(int j = 0; j < qtColunasA; j++) {
            fscanf(fentrada1, "%f", &matA[i][j]);
        }
    }
    fclose(fentrada1);


    matB = (float**) malloc(sizeof(float*) * qtLinhasB);
    if(matB == NULL)
        return -1;
    
    for(int i = 0; i < qtLinhasB; i++) {
        matB[i] = (float*) malloc(sizeof(float) * qtColunasB);
        if(matB[i] == NULL)
            return -1;
        
        for(int j = 0; j < qtColunasB; j++) {
            fscanf(fentrada2, "%f", &matB[i][j]);
        }
    }
    fclose(fentrada2);


    matRes = (float**) malloc(sizeof(float*) * qtLinhasA);
    if(matRes == NULL)
        return -1;

    
    for(int i = 0; i < qtLinhasA; i++) {
        matRes[i] = (float*) malloc(sizeof(float) * qtColunasB);
        if(matRes[i] == NULL)
            return -1;
    }

    
    double tempo = (double) (clock() - start) / CLOCKS_PER_SEC;
    printf("\n\nTempo de entrada de dados: %.10f", tempo);

    tempo_total += tempo;


    const int num_execucoes = 5;
    start = clock();

    for(int i = 0; i < num_execucoes; i++) {
        if(num_threads == 0) {
            for(int i = 0; i < qtColunasB; i++) {
                calculaColuna(i, matA, matB, matRes, qtLinhasA, qtColunasA, qtColunasB);
            }
        } else {
            pthread_t tid_vector[num_threads];
            int size_per_thread = qtColunasB / num_threads;
            tv_Args *arg;

            for(int t = 0; t < num_threads; t++) {
                arg = getArgs(
                    t+1, 
                    matA, 
                    matB, 
                    matRes, 
                    qtLinhasA,
                    qtColunasA, 
                    qtColunasB, 
                    size_per_thread
                );

                if(arg == NULL)
                    return -1;
                
                if(pthread_create(&tid_vector[t], NULL, calculaColunaThread, (void *) arg)) {
                    cout << "--ERRO: pthread_create()\n";
                }
            }

            for(int t = 0; t < num_threads; t++) {
                if(pthread_join(tid_vector[t], NULL)) {
                    cout << "--ERRO: pthread_join()\n";
                }
            }
        }

    }


    tempo = ((double) (clock() - start) / CLOCKS_PER_SEC) / num_execucoes;
    printf("\nTempo de processamento: %.10f", tempo);

    tempo_processamento = tempo;
    tempo_total += tempo;


    start = clock();

    for(int i = 0; i < qtLinhasA; i++) {
        for(int j = 0; j < qtColunasB; j++) {
            fprintf(fsaida, "%f ", matRes[i][j]);
        }
        
        fprintf(fsaida, "\n");
    }

    fclose(fsaida);

    for(int i = 0; i < qtLinhasA; i++) {
        free(matA[i]);
        free(matRes[i]);
    }

    for(int i = 0; i < qtLinhasB; i++) {
        free(matB[i]);
    }

    free(matA);
    free(matB);
    free(matRes);

    tempo = (double) (clock() - start) / CLOCKS_PER_SEC;
    printf("\nTempo de Saída de Dados: %.10f", tempo);


    tempo_total += tempo;

    cout << "\n---------\nTempo total: " << tempo_total << "  -  Tempo de processamento: " << tempo_processamento;


    if(num_threads == 1) {
        double possivel_tempo_paralelo = tempo_total - tempo_processamento + tempo_processamento / 2;
        double ganho = tempo_total / possivel_tempo_paralelo;
        double porcentagem_economia_tempo = 100 - possivel_tempo_paralelo / tempo_total * 100;

        cout << "\n\nEm execução paralela com dois núcleos, possívelmente o programa seria " << ganho << " vezes mais rápido";
        cout << " (" << porcentagem_economia_tempo << "% de economia de tempo, com execução em " << possivel_tempo_paralelo << " segundos).";
        
    }

    cout << "\n\n";

}



void calculaColuna (
    int coluna, 
    float **matA, 
    float **matB, 
    float **matRes,
    int qtLinhasA,
    int qtColunasA,
    int qtColunasB
) {   

    for(int i = 0; i < qtLinhasA; i++) {
        float m = 0;

        for(int j = 0; j < qtColunasA; j++) {
            m += matA[i][j] * matB[j][coluna];
        }
        
        matRes[i][coluna] = m;
    }
}

void *calculaColunaThread(void *_arg) {
    tv_Args *arg = (tv_Args *) _arg;

    for(int i = arg->colunaMin; i <= arg->colunaMax; i++) {
        calculaColuna(
            i, 
            arg->matA, 
            arg->matB, 
            arg->matRes, 
            arg->qtLinhasA,
            arg->qtColunasA,
            arg->qtColunasB
        );
    }

    free(arg);
    pthread_exit(NULL);

    return 0;
}

tv_Args* getArgs(
    int idThread, 
    float **matA, 
    float **matB, 
    float **matRes,
    int qtLinhasA,
    int qtColunasA,
    int qtColunasB, 
    int sizePerThread
) {
    tv_Args *arg;
    
    arg = (tv_Args *) malloc(sizeof(tv_Args));
    if(arg == NULL) {
        cout << "--ERRO: malloc()\n";
        return NULL;
    }

    arg->idThread = idThread;
    arg->matA = matA;
    arg->matB = matB;
    arg->matRes = matRes;
    arg->qtLinhasA = qtLinhasA;
    arg->qtColunasA = qtColunasA;
    arg->qtColunasB = qtColunasB;
    arg->colunaMin = (idThread - 1) * sizePerThread;
    arg->colunaMax = idThread * sizePerThread - 1;

    return arg;
}