#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <iostream>
#include <pthread.h>

#define NTHREADS 10

using namespace std;


typedef struct {
    int idThread;
    int *vet;
    int min;
    int max;
} tv_Args;

void *IncVector (void *arg);

int processVector(int n);
int* getVector(int n);
void printVector(int *vet, int n);
tv_Args* getArgs(int idThread, int *vet, int sizePerThread);


int main() {
    cout << "\n";
    //int n = (random() % 90) + 10;

    /*
    for(int i = 2; i <= 100; i += 2) {
        int ret = processVector(i);
        if(ret == -1)
            return -1;
    }*/

    if(processVector(50) == -1) {
        return -1;
    }
    

    cout << "\n\n";
}

int processVector(int n ) {
    cout << "\n";
    int sizePerThread = n / NTHREADS;

    tv_Args *arg;
    int *vet = getVector(n);
    
    printVector(&vet[0], n);


    if(vet == NULL)
        return -1;

    pthread_t tid_vector[NTHREADS];

    cout << "n: " << n << ". " << 
        "Size per thread: " << sizePerThread;
    
    for(int t = 0; t < NTHREADS; t++) {
        arg = getArgs(t+1, vet, sizePerThread);

        if(arg == NULL)
            return -1;
        
        if(pthread_create(&tid_vector[t], NULL, IncVector, (void *) arg)) {
            cout << "--ERRO: pthread_create()\n";
            return -1;
        }
    }

    for(int t = 0; t < NTHREADS; t++) {
        if(pthread_join(tid_vector[t], NULL)) {
			printf("--ERRO: pthread_join() \n"); 
			return -1; 
        }
    }

	printf("\n\n--Thread principal terminou\n");

    printVector(&vet[0], n);


    free(vet);
    

	getchar();
	pthread_exit(NULL);
    return 0;
}

int* getVector(int n) {
    int *vet = (int *) malloc(n * sizeof(int));

    if(vet == NULL) {
        cout << "--ERRO: malloc()\n";
        return NULL;
    }

    for(int i = 0; i < n; i++) {
        vet[i] = i + 100;
    }

    return &vet[0];
}

void printVector(int *vet, int n) {
    cout << "Imprimindo vetor...";

    for(int i = 0; i < n; i++) {
        cout << "\n  vet[" << i << "] = " << vet[i];
    }

    cout << "\n\nFim da impressão do vetor\n\n\n";
}


tv_Args* getArgs(int idThread, int *vet, int sizePerThread) {
    tv_Args *arg;
    
    arg = (tv_Args *) malloc(sizeof(tv_Args));

    if(arg == NULL) {
        cout << "--ERRO: malloc()\n";
        return NULL;
    }

    arg->idThread = idThread;
    arg->vet = &vet[0];
    arg->min = (idThread-1) * sizePerThread;
    arg->max = (idThread) * sizePerThread - 1;


    return arg;
}


void *IncVector (void *_arg) {
    tv_Args *arg = (tv_Args *) _arg;

    cout << "\n----Thread id: " << arg->idThread << ". min: " << 
        arg->min << ". max: " << arg->max <<"";

    
    for(int i = arg->min; i <= arg->max; i++) {
        *(arg->vet + i) += 10;
    }

    free(arg);

    pthread_exit(NULL);
    return 0;
}