

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <iostream>
#include <chrono>

#define NUM_MEDICOES 100

using namespace std;

typedef struct {
    int idThread;
    int *vet;
    int min;
    int max;
} tv_Args;



int* generateVector(int n);

tv_Args* getArgs(int idThread, int *vet, int sizePerThread);

chrono::duration<double> teste(int tamVetor, int numThreads);

void printVector(int *vet, int n);


void *IncVectorThread (void *arg);
void incVectorSeq(int* vet, int n);



int main() {
    cout << "\n";

    //Os tamanhos dos vetores devem ser multiplos dos numeros de threads
    int tamanhos[] = {
       100000000
    };


    int qtThreads[] = {
        4, 8
    };

    for(int i = 0; i < sizeof(tamanhos) / sizeof(int); i++) {
        for(int j = 0; j < sizeof(qtThreads) / sizeof(int); j++) {
            chrono::duration<double> tempo;
            tempo = teste(tamanhos[i], qtThreads[j]);

            if(tempo == chrono::duration<double>::zero()) {
                return -1;
            }

            printf("Tamanho do vetor: %d      \tThreads: %d", tamanhos[i], qtThreads[j]);
            printf("\tTempo gasto: \t%.10f\n", tempo.count());
        }
    }
}

chrono::duration<double> teste(int tamVector, int numThreads) {
    int sizePerThread = tamVector / numThreads;
    
    tv_Args *arg;
    int *vet = generateVector(tamVector);

    //printVector(vet, tamVector);
    if(vet == NULL) {
        return chrono::duration<double>::zero();
    }

    pthread_t tid_vector[numThreads];
    chrono::duration<double> soma_tempo = chrono::duration<double>::zero();

    for(int i = 0; i < NUM_MEDICOES; i++) {
        auto start = chrono::high_resolution_clock::now();

        if(numThreads == 1) {
            incVectorSeq(vet, tamVector);
        } else {
            for(int t = 0; t < numThreads; t++) {
                arg = getArgs(t+1, vet, sizePerThread);

                if(arg == NULL)
                    return chrono::duration<double>::zero();
                
                if(pthread_create(&tid_vector[t], NULL, IncVectorThread, (void *) arg)) {
                    
                    cout << "--ERRO: pthread_create()\n";
                    return chrono::duration<double>::zero();
                }
            }
            
            for(int t = 0; t < numThreads; t++) {
                if(pthread_join(tid_vector[t], NULL)) {
                    cout << "--ERRO: pthread_join()\n";
                    return chrono::duration<double>::zero();
                }
            }
        }
    
        auto finish = chrono::high_resolution_clock::now();
        soma_tempo += (finish - start);
    }

   // printVector(vet, tamVector);

    free(vet);
    return soma_tempo / NUM_MEDICOES;
}




void *IncVectorThread (void *_arg) {
    tv_Args *arg = (tv_Args *) _arg;
    
    for(int i = arg->min; i <= arg->max; i++) {
        *(arg->vet + i) += 10;
    }

    free(arg);

    pthread_exit(NULL);
    return 0;
}


void incVectorSeq(int* vet, int n) {
    for(int i = 0; i < n; i++) {
        *(vet + i) += 10;
    }
}


int* generateVector(int n) {
    int *vet = (int *) malloc(n * sizeof(int));
    
    if(vet == NULL) {
        cout << "--ERRO: malloc()\n";
        return NULL;
    }

    for(int i = 0; i < n; i++) {
        vet[i] = i + 100;
    }

    return &vet[0];
}


void printVector(int *vet, int n) {
    cout << "Imprimindo vetor...";

    for(int i = 0; i < n; i++) {
        cout << "\n  vet[" << i << "] = " << *(vet + i);
    }

    cout << "\n\nFim da impressao do vetor\n\n\n";
}

tv_Args* getArgs(int idThread, int *vet, int sizePerThread) {
    tv_Args *arg;
    
    arg = (tv_Args *) malloc(sizeof(tv_Args));
    if(arg == NULL) {
        cout << "--ERRO: malloc()\n";
        return NULL;
    }

    arg->idThread = idThread;
    arg->vet = &vet[0];
    arg->min = (idThread-1) * sizePerThread;
    arg->max = (idThread) * sizePerThread - 1;

    return arg;
}
