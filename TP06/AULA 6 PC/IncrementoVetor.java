// Objeto do vetor
class Vetor {
    int vet[];

    public Vetor(int n) {
        this.vet = new int[n];

        for(int i = 0; i < n; i++) {
            vet[i] = i;
        }
    }

    /**
     * Incremento do vetor
     * 
     * Não é necessário sincronização, pois as threads serão
     * responsáveis por acessar posições diferentes
     */
    public void inc(int pos, int value) {
        this.vet[pos] += value;
    }

    public String getVet() {
        StringBuilder sb = new StringBuilder();

        for(int i = 0; i < this.vet.length; i++) {
            sb.append(vet[i]);
            sb.append("\n");
        }

        return sb.toString();
    }

}

class Range {
    int min, max;

    public Range(int min, int max) {
        this.min = min;
        this.max = max;
    }
}

// Classe das threads
class T extends Thread {
    int id;
    Vetor vet;
    Range range;

    public T(int tid, Vetor vet, Range range) {
        this.id = tid;
        this.vet = vet;
        this.range = range;

        //System.out.println("Thread " + this.id + " construida.");
        //System.out.println("Intervalo: " + range.min + " - " + range.max);

    }

    // Execução das threads
    public void run() {
        for(int i = range.min; i <= range.max; i++) {
            this.vet.inc(i, 10);
        }
    }
}

class IncrementoVetor {
    int qtThreads, tamVetor;
    int tamVetorPorThread;

    /**
     * String[] args
     * 
     * args[0] - Quantidade de threads
     * args[1] - Tamanho do vetor
     */
    public static void main(String[] args) throws InterruptedException {
        if(args.length < 2) {
            System.out.println("Argumentos inválidos");
            System.out.println("Uso: [qtThreads] [tamVetor] \n");

            return;
        }

        IncrementoVetor programa = new IncrementoVetor();

        programa.qtThreads = Integer.parseInt(args[0]);
        programa.tamVetor = Integer.parseInt(args[1]);
        programa.tamVetorPorThread = programa.tamVetor / programa.qtThreads; 

        programa.executar();
    }

    public void executar() throws InterruptedException {
        Thread[] threads = new Thread[qtThreads];
        
        // Alocação do vetor
        Vetor vet = new Vetor(tamVetor);

        //System.out.println("Antes da execução: ");
        //System.out.println(vet.getVet());


        for(int i = 0; i < qtThreads; i++) {
            int min = this.tamVetorPorThread * i;
            int max = this.tamVetorPorThread * (i+1) - 1;

            threads[i] = new T(i, vet, new Range(min, max));
        }

        double start = System.nanoTime();

        //Inicialização das threads
        for(int i = 0; i < qtThreads; i++) {
            threads[i].start();
        }

        // Espera pela conclusão das threads
        for(int i = 0; i < qtThreads; i++) {
            threads[i].join();
        }


        double time = (double) System.nanoTime() - start;
        //System.out.println("Resultado: ");
        //System.out.println(vet.getVet());

        System.out.println("[Tempo = " + time / 1000000 + " ms]\n");
    }
}