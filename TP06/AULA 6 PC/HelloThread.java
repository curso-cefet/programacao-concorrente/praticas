//--PASSO 1: criar uma classe que implementa a interface Runnable 
class Hello implements Runnable {

    String msg;

    //--construtor
    public Hello(String m) {
        msg = m;
    }

    //--metodo executado pela thread
    public void run() {
        System.out.println(msg + " --  Início");
        for (int i = 0; i < 1000000; i++) {
        }
        System.out.println(msg + " --  Fim");
    }
}

//--classe do metodo main
class HelloThread {

    static final int N = 10;

    public static void main(String[] args) {
        //--reserva espaço para um vetor de threads
        Thread[] threads = new Thread[N];

        //--PASSO 2: transformar o objeto Runnable em Thread
        for (int i = 0; i < threads.length; i++) {
            final String m = "Hello World da thread: " + i;
            //System.out.println("--Cria a thread " + i);
            threads[i] = new Thread(new Hello(m));
        }

        //--PASSO 3: iniciar a thread
        for (int i = 0; i < threads.length; i++) {
            threads[i].start();
        }

        //--PASSO extra: esperar pelo termino das threads
        
        for (int i=0; i<threads.length; i++) {
         try { threads[i].join(); } catch (InterruptedException e) { return; }
        }
         
        System.out.println("Terminou");
    }
}


//Relatório: Apenas falar que o inicio e fim das threads ocorrem de forma desordenada
//Depois falar que ao descomentar as linhas de 42 a 46, a parte final da thread principal
// espera pelas outras threads (join)

//Colocar alguns exemplos de saída:

/**
 * 
 * 
Hello World da thread: 0 --  Início
Hello World da thread: 2 --  Início
Hello World da thread: 1 --  Início
Hello World da thread: 0 --  Fim
Hello World da thread: 3 --  Início
Hello World da thread: 4 --  Início
Hello World da thread: 1 --  Fim
Hello World da thread: 4 --  Fim
Hello World da thread: 3 --  Fim
Hello World da thread: 5 --  Início
Hello World da thread: 6 --  Início
Hello World da thread: 6 --  Fim
Hello World da thread: 5 --  Fim
Hello World da thread: 7 --  Início
Hello World da thread: 2 --  Fim
Terminou
Hello World da thread: 7 --  Fim
Hello World da thread: 9 --  Início
Hello World da thread: 9 --  Fim
Hello World da thread: 8 --  Início
Hello World da thread: 8 --  Fim




Hello World da thread: 0 --  Início
Hello World da thread: 1 --  Início
Hello World da thread: 2 --  Início
Hello World da thread: 2 --  Fim
Hello World da thread: 3 --  Início
Hello World da thread: 1 --  Fim
Hello World da thread: 5 --  Início
Hello World da thread: 5 --  Fim
Hello World da thread: 4 --  Início
Hello World da thread: 4 --  Fim
Hello World da thread: 0 --  Fim
Hello World da thread: 3 --  Fim
Hello World da thread: 6 --  Início
Hello World da thread: 6 --  Fim
Terminou
Hello World da thread: 7 --  Início
Hello World da thread: 9 --  Início
Hello World da thread: 7 --  Fim
Hello World da thread: 9 --  Fim
Hello World da thread: 8 --  Início
Hello World da thread: 8 --  Fim



-----

Hello World da thread: 0 --  Início
Hello World da thread: 1 --  Início
Hello World da thread: 1 --  Fim
Hello World da thread: 0 --  Fim
Hello World da thread: 3 --  Início
Hello World da thread: 3 --  Fim
Hello World da thread: 2 --  Início
Hello World da thread: 2 --  Fim
Hello World da thread: 4 --  Início
Hello World da thread: 4 --  Fim
Hello World da thread: 5 --  Início
Hello World da thread: 6 --  Início
Hello World da thread: 5 --  Fim
Hello World da thread: 6 --  Fim
Hello World da thread: 7 --  Início
Hello World da thread: 7 --  Fim
Hello World da thread: 8 --  Início
Hello World da thread: 8 --  Fim
Hello World da thread: 9 --  Início
Hello World da thread: 9 --  Fim
Terminou

Hello World da thread: 0 --  Início
Hello World da thread: 0 --  Fim
Hello World da thread: 2 --  Início
Hello World da thread: 1 --  Início
Hello World da thread: 1 --  Fim
Hello World da thread: 2 --  Fim
Hello World da thread: 3 --  Início
Hello World da thread: 3 --  Fim
Hello World da thread: 4 --  Início
Hello World da thread: 4 --  Fim
Hello World da thread: 6 --  Início
Hello World da thread: 6 --  Fim
Hello World da thread: 5 --  Início
Hello World da thread: 5 --  Fim
Hello World da thread: 7 --  Início
Hello World da thread: 7 --  Fim
Hello World da thread: 8 --  Início
Hello World da thread: 8 --  Fim
Hello World da thread: 9 --  Início
Hello World da thread: 9 --  Fim
Terminou
 */
