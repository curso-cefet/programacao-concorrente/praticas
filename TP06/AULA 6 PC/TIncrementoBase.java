//classe da estrutura de dados (recurso) compartilhado entre as threads
class S {

    //recurso compartilhado
    int r;

    //construtor
    public S() {
        this.r = 0;
    }

    
    
    //operacao sobre o recurso compartilhado
    public void inc() {
        this.r++;
    } 
   
    /*public void inc() { 
     synchronized(this) {
     this.r++; 
     }
     }*/

    /*public synchronized void inc() { 
     this.r++; 
     }*/
    //operacao sobre o recurso compartilhado
    public int get() {
        return this.r;
    }
    /*
    public int get() { 
     synchronized(this) {
     return this.r; 
     }
    } */   
    /*public synchronized int get() { 
     return this.r; 
     }*/
}

//classe que estende Thread e implementa a tarefa de cada thread do programa 
class T extends Thread {

    //identificador da thread
    int id;
	
    //objeto compartilhado com outras threads
    S s;

    //construtor
    public T(int tid, S s) {
        this.id = tid;
        this.s = s;
    }

    //metodo main da thread
    public void run() {
        System.out.println("Thread " + this.id + " iniciou!");
        for (int i = 0; i < 10000000; i++) {
            this.s.inc();
        }
        System.out.println("Thread " + this.id + " terminou!");
    }
}

//classe da aplicacao
class TIncrementoBase {

    static final int N = 2;

    public static void main(String[] args) {
        //reserva espaco para um vetor de threads
        Thread[] threads = new Thread[N];

        //cria uma instancia do recurso compartilhado entre as threads
        S s = new S();

        //cria as threads da aplicacao
        for (int i = 0; i < threads.length; i++) {
            threads[i] = new T(i, s);
        }

        double start = System.currentTimeMillis();

        //inicia as threads
        for (int i = 0; i < threads.length; i++) {
            threads[i].start();
        }

        //espera pelo termino de todas as threads
        for (int i = 0; i < threads.length; i++) {
            try {
                threads[i].join();
            } catch (InterruptedException e) {
                return;
            }
        }


        double time = System.currentTimeMillis() - start;

        System.out.println("Valor de s = " + s.get() + "  [Tempo = " + time + " ms]");
    }
}




// Seções críticas: Bloco de código que altera o valor de 's'
// Saída esperada: 20000000


/**
 * 
Execução att 3 (calcular o tempo):

Thread 0 iniciou!
Thread 1 iniciou!
Thread 0 terminou!
Thread 1 terminou!
Valor de s = 19860981  [Tempo = 25.0 ms]

Thread 0 iniciou!
Thread 1 iniciou!
Thread 1 terminou!
Thread 0 terminou!
Valor de s = 11130865  [Tempo = 11.0 ms]

Thread 1 iniciou!
Thread 0 iniciou!
Thread 1 terminou!
Thread 0 terminou!
Valor de s = 11138107  [Tempo = 10.0 ms]

Thread 0 iniciou!
Thread 1 iniciou!
Thread 0 terminou!
Thread 1 terminou!
Valor de s = 14369205  [Tempo = 15.0 ms]

Thread 0 iniciou!
Thread 1 iniciou!
Thread 0 terminou!
Thread 1 terminou!
Valor de s = 10064039  [Tempo = 19.0 ms]

Thread 0 iniciou!
Thread 1 iniciou!
Thread 0 terminou!
Thread 1 terminou!
Valor de s = 10033548  [Tempo = 18.0 ms]

Thread 0 iniciou!
Thread 1 iniciou!
Thread 0 terminou!
Thread 1 terminou!
Valor de s = 10060764  [Tempo = 19.0 ms]

Thread 0 iniciou!
Thread 1 iniciou!
Thread 1 terminou!
Thread 0 terminou!
Valor de s = 14073799  [Tempo = 18.0 ms]

Valores impressos não foram os esperados porque a não havia implementação de sincronização
por exclusão mútua na seção crítica




------
Execução att4

Thread 0 iniciou!
Thread 1 iniciou!
Thread 0 terminou!
Thread 1 terminou!
Valor de s = 20000000  [Tempo = 1504.0 ms]


Thread 0 iniciou!
Thread 1 iniciou!
Thread 1 terminou!
Thread 0 terminou!
Valor de s = 20000000  [Tempo = 1241.0 ms]


Thread 0 iniciou!
Thread 1 iniciou!
Thread 1 terminou!
Thread 0 terminou!
Valor de s = 20000000  [Tempo = 1472.0 ms]


Thread 1 iniciou!
Thread 0 iniciou!
Thread 1 terminou!
Thread 0 terminou!
Valor de s = 20000000  [Tempo = 1574.0 ms]

Thread 0 iniciou!
Thread 1 iniciou!
Thread 1 terminou!
Thread 0 terminou!
Valor de s = 20000000  [Tempo = 1747.0 ms]

Thread 0 iniciou!
Thread 1 iniciou!
Thread 1 terminou!
Thread 0 terminou!
Valor de s = 20000000  [Tempo = 936.0 ms]


Thread 1 iniciou!
Thread 0 iniciou!
Thread 1 terminou!
Thread 0 terminou!
Valor de s = 20000000  [Tempo = 1217.0 ms]


Thread 0 iniciou!
Thread 1 iniciou!
Thread 1 terminou!
Thread 0 terminou!
Valor de s = 20000000  [Tempo = 913.0 ms]
 */
