// Monitor
class Buffer {

    int N;
    int[] buffer;
    int count = 0, in = 0, out = 0;   //variaveis compartilhadas



    // Construtor
    Buffer(int N) {
        this.N = N;
        buffer = new int[N];

        for (int i = 0; i < N; i++) {
            buffer[i] = -1;
        }
    } // inicia o buffer


    // Insere um item
    public synchronized void Insere(int item,  Execucao ex) {
        if(PC.numTeste == 2 || PC.numTeste == 4)
            PC.checarTempo();

        try {
            while(count == N) {
                ex.addFalha();
                if(PC.numTeste == 1 || PC.numTeste == 3)
                    System.out.println("[" + ex.getTid() + "] Falha ao produzir: Buffer cheio & " + this.count + " & \\\\ \\hline");
                
                wait();
            }

            buffer[in] = item;
            in = (in + 1) % N;

            count++;
            ex.addSucesso();

            if(PC.numTeste == 1 || PC.numTeste == 3)
                System.out.println("[" + ex.getTid() + "] Item produzido & " + this.count + " &  \\\\ \\hline");


            notifyAll();
        } catch (InterruptedException e) {
        }
    }

    // Remove um item
    public synchronized void Remove(Execucao ex) {

        if(PC.numTeste == 2 || PC.numTeste == 4)
            PC.checarTempo();

        int aux;
        try {
            /**
             * Para a atividade 7, impõe a condição de esperar o buffer
             * tornar-se cheio antes de consumir, além de consumí-lo por 
             * completo
             */

            while( count == 0 || ((PC.numTeste == 3 || PC.numTeste == 4) && count < N ) )   {
                ex.addFalha();
                if(PC.numTeste == 1)
                    System.out.println("& " + this.count + " & [" + ex.getTid() + "] Falha ao consumir: Buffer vazio \\\\ \\hline");
                
                if(PC.numTeste == 3)
                    System.out.println("& " + this.count + " & [" + ex.getTid() + "] Falha ao consumir: Buffer não está cheio \\\\ \\hline");
                wait();
            }


            if(PC.numTeste == 1 || PC.numTeste == 2) {
                aux = buffer[out % N];

                out = (out + 1) % N;
                count--;
                ex.addSucesso();


                if(PC.numTeste == 1 || PC.numTeste == 3)
                    System.out.println("& " + this.count + " & [" + ex.getTid() + "] Item consumido  \\\\ \\hline");


            } else {
                while(count > 0) {
                    aux = buffer[out % N];
                    //Processa item (aux)

                    out = (out + 1) % N;
                    count--;
                    ex.addSucesso();


                    if(PC.numTeste == 1 || PC.numTeste == 3)
                        System.out.println("& " + this.count + " & [" + ex.getTid() + "] Item consumido  \\\\ \\hline");


                }
            }


            
            notifyAll();

        } catch (InterruptedException e) {
        }
    }
}


abstract class Execucao extends Thread {
    abstract int getTid();



    private int falhas = 0;
    private int sucessos = 0;
    
    void addFalha() {
        this.falhas++;
    }
    void addSucesso() {
        this.sucessos++;
    }
    
    int getFalhas() {
        return this.falhas;
    }
    int getSucessos() {
        return this.sucessos;
    }
}

//--------------------------------------------------------
// Consumidor
class Consumidor extends Execucao {

    int id;
    int delay;
    Buffer buffer;

    // Construtor
    Consumidor(int id, int delayTime, Buffer b) {
        this.id = id;
        this.delay = delayTime;
        this.buffer = b;
    }

    // Metodo executado pela thread
    public void run() {

        if(PC.numTeste == 1 || PC.numTeste == 3)
            System.out.println("&" + this.buffer.count + " & [" + id + 
                    "] Consumidor inicializado \\\\ \\hline");


        try {
            for (;;) {                
                this.buffer.Remove(this);
                sleep(this.delay); //...simula o tempo para fazer algo com o item retirado

            }
        } catch (InterruptedException e) {
            return;
        }
    }

    int getTid() {
        return this.id;
    }
}

//--------------------------------------------------------
// Produtor
class Produtor extends Execucao {


    int id;
    int delay;
    Buffer buffer;

    // Construtor
    Produtor(int id, int delayTime, Buffer b) {
        this.id = id;
        this.delay = delayTime;
        this.buffer = b;
    }

    // Metodo executado pelo thread
    public void run() {
        if(PC.numTeste == 1 || PC.numTeste == 3)
            System.out.println("[" + id + 
            "] Produtor inicializado &" + this.buffer.count + " &  \\\\ \\hline");
        
        
        try {
            for (;;) {
                this.buffer.Insere(this.id, this); //simplificacao: insere o proprio ID
                sleep(this.delay);


            }
        } catch (InterruptedException e) {
            return;
        }
    }


    int getTid() {
        return this.id;
    }
}

//--------------------------------------------------------
// Classe principal
class PC {
    static int P, C;
    static Consumidor[] cons;
    static Produtor[] prod;
    static int numTeste;
    static long controleTempo;

    static void checarTempo() {
        long atual = System.currentTimeMillis();
        if(atual - controleTempo   >= 60000) {
            controleTempo = atual;

            System.out.println("Produtor");

            for (int i = 0; i < P; i++) {
                System.out.println("\n" + "[" + prod[i].getTid() + "] Sucesso: " + prod[i].getSucessos());
                System.out.println("[" + prod[i].getTid() + "] Falha: " + prod[i].getFalhas());
            }

            System.out.println("\n\n\nConsumidor");
            for (int i = 0; i < C; i++) {
                System.out.println("\n" + "[" + cons[i].getTid() + "] Sucesso: " + cons[i].getSucessos());
                System.out.println("[" + cons[i].getTid() + "] Falha: " + cons[i].getFalhas());
                
            }
            
        }
    }

    /**
     * String[] args
     * 
     * args[0] - Quantidade de produtores
     * args[1] - Quantidade de consumidores
     * args[2] - Tamanho do buffer
     * args[3] - Tipo de teste
     *          1,2 ----- Atividade 6
     *                      1---------Etapa 1 
     *                      2---------Etapa 2
     *          3,4 ----- Atividade 7
     *                      3---------Etapa 1 
     *                      4---------Etapa 2
     *      
     * 
     *      Atividade 6 - Modo normal de funcionamento
     *      Atividade 7 - Consumidor só consome o buffer inteiro, 
     *                    e não apenas um item por vez
     * 
        impressoesTempo = 0;
     *      Etapa 1 - Geração de logs - Execução por tempo reduzido (dois segundos)
     *      Etapa 2 - Análise estatística - Avaliar quantas vezes cada produtor
     *                ou consumidor teve sucesso ao executar sua tarefa
     *                 - Execução por tempo maior (cinco minutos)
     *      
     *      
     */
    public static void main(String[] args) {
        if(args.length < 4) {
            System.out.println("Argumentos inválidos");
            System.out.println("Uso: [qtProdutores] [qtConsumidores] [tamBuffer] [numTeste] \n");

            return;
        }
        
        P = Integer.parseInt(args[0]);
        C = Integer.parseInt(args[1]);
        int tamBuffer = Integer.parseInt(args[2]);
        numTeste = Integer.parseInt(args[3]);

        controleTempo = System.currentTimeMillis();

        int i;
        Buffer buffer = new Buffer(tamBuffer);      // Monitor
        cons = new Consumidor[C];   // Consumidores
        prod = new Produtor[P];       // Produtores

        for (i = 0; i < C; i++) {
            cons[i] = new Consumidor(i + 1, 1000, buffer);
            cons[i].start();
        }
        for (i = 0; i < P; i++) {
            prod[i] = new Produtor(i + 1, 1000, buffer);
            prod[i].start();
        }
    }
}
