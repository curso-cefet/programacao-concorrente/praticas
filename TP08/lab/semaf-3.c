#include <stdio.h>
#include <stdlib.h> 
#include <pthread.h>
#include <semaphore.h>

#define NTHREADS 4



//semáforos para sincronizar a ordem de execução das threads
sem_t condbye, em;     

//Quantidade das threads iniciais (1 e 2) que foram executadas
int count = 0;

void *t1 (void *threadid) {
  int *tid = (int*) threadid;
  printf("[T%d] olá, tudo bem?\n", *tid);
  
  sem_wait(&em);
  count++;

  if(count == 2) {
    sem_post(&condbye);
    sem_post(&condbye);
  }
  sem_post(&em);

  pthread_exit(NULL);
}

void *t2 (void *threadid) {
  int *tid = (int*) threadid;
  printf("[T%d] hello!\n", *tid);
  

  sem_wait(&em);
  count++;

  if(count == 2) {
    sem_post(&condbye);
    sem_post(&condbye);
  }
  sem_post(&em);

  pthread_exit(NULL);
}

void *t3 (void *threadid) {
  int *tid = (int*) threadid;

  sem_wait(&condbye);
  printf("[T%d] até mais tarde.\n", *tid);
  


  pthread_exit(NULL);
}

void *t4 (void *threadid) {
  int *tid = (int*) threadid;

  sem_wait(&condbye);
  printf("[T%d] tchau!\n", *tid);
  
  

  pthread_exit(NULL);
}

//Função principal
int main(int argc, char *argv[]) {
  pthread_t tid[NTHREADS];
  int *id[NTHREADS], t;


  for (t=0; t<NTHREADS; t++) {
    if ((id[t] = (int*) malloc(sizeof(int))) == NULL) {
       pthread_exit(NULL); return 1;
    }
    *id[t] = t+1;
  }

  //Inicia os semáforos
  sem_init(&condbye, 0, 0);
  sem_init(&em, 0, 1);
  

  //Inicializa as threads
  if (pthread_create(&tid[0], NULL, t1, (void *)id[0])) { printf("--ERRO: pthread_create()\n"); exit(-1); }
  if (pthread_create(&tid[1], NULL, t2, (void *)id[1])) { printf("--ERRO: pthread_create()\n"); exit(-1); }
  if (pthread_create(&tid[2], NULL, t3, (void *)id[2])) { printf("--ERRO: pthread_create()\n"); exit(-1); }
  if (pthread_create(&tid[3], NULL, t4, (void *)id[3])) { printf("--ERRO: pthread_create()\n"); exit(-1); }



  for (t=0; t<NTHREADS; t++) {
    if (pthread_join(tid[t], NULL)) {
         printf("--ERRO: pthread_join() \n"); exit(-1); 
    } 
  }

  pthread_exit(NULL);
}
