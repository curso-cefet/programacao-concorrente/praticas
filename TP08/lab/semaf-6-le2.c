#include <stdio.h>
#include <stdlib.h> 
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>

#define NTHREADS 4

#define NLEITORES 2
#define NESCRITORES 2

#define DELAY_ESPERA_US 1000000
#define DELAY_INTERVALO_US 1000000
#define DELAY_ESCRITA_US 2000000
#define DELAY_LEITURA_US 1000000

#define TIMEOUT_US 10000000

//#define TIMEOUT_US 100000

typedef struct {
    int count;
    int tId;
} t_dados;


t_dados *dados;
    
/**
 * qtL = quantidade de leitores que entraram
 * qtE = quantidade de escritores que entraram
 * qtE2 = quantidade de escritores que entraram ou querem entrar
 */
int qtL, qtE;

int leitorBloqueouEscritor;

//Semárofo de exclusão mútua
sem_t em;     

int encerrarExecucao;

int escritorQuerEntrar;

int tentativaLeituras[NLEITORES + 1];
int tentativaEscritas[NESCRITORES + 1];

int totalLeituras[NLEITORES + 1];
int totalEscritas[NESCRITORES + 1];


// Método que executa o processo de leitura. Obs: Possui seção crítica, por isso
// deve ser chamado dentro da exclusão mútua global
void entraLeitor(int tid) {
    sem_wait(&em);

    printf("[L%d] Leitor %d tenta entrar\n", tid, tid);

    // Há escritores escrevendo ou querendo escrever
    while (qtE > 0 || escritorQuerEntrar > 0) {
        printf("[L%d] Leitor %d está esperando... [%d escritores com prioridade estão em espera]\n", tid, tid, escritorQuerEntrar);
        // Libera o recurso
        sem_post(&em);   
        
        tentativaLeituras[tid]++;
        usleep(DELAY_ESPERA_US);

        // Após um tempo de espera, retoma o recurso para verificar novamente as condições
        sem_wait(&em);   
    }
    tentativaLeituras[tid]++;
    totalLeituras[tid]++;
 
    // Leitores que estão lendo
    qtL++;

    printf("[L%d] Leitor %d entrou... Total de %d lendo\n", tid, tid, qtL);

    sem_post(&em);   
}

void saiLeitor(int tid) {
    sem_wait(&em);
    
    // Leitores que estão lendo
    qtL--;

    printf("[L%d] Leitor %d saiu, restam %d...\n", tid, tid, qtL);

    sem_post(&em);   
}

// Método de preparação para o processo de escrita.
void entraEscritor(int tid) {
    sem_wait(&em);

    printf("[E%d] Escritor %d tenta entrar\n", tid, tid);

    int sinalizouTentativaEntrar = 0;

    // Há algum leitor ou escritor ativo
    while (qtE > 0 || qtL > 0) {
        
        if(sinalizouTentativaEntrar == 0) {
            sinalizouTentativaEntrar = 1;
            escritorQuerEntrar++;
        }
        
        printf("[E%d] Escritor %d está esperando...\n", tid, tid);
        // Libera o recurso
        sem_post(&em);   
        
        tentativaEscritas[tid]++;
        usleep(DELAY_ESPERA_US);

        // Após um tempo de espera, retoma o recurso para verificar novamente as condições
        sem_wait(&em);   
    }
    tentativaEscritas[tid]++;
    totalEscritas[tid]++;

    // Quantidade de escritores ativos
    qtE++;

    if(sinalizouTentativaEntrar == 1) {
        escritorQuerEntrar--;
    }
    
    printf("[E%d] Escritor %d entrou... Total de %d escrevendo\n", tid, tid, qtE);

    sem_post(&em);
}

void saiEscritor(int tid) {
    sem_wait(&em);
    
    // Quantidade de escritores ativos
    qtE--;

    printf("[E%d] Escritor %d saiu, restam %d...\n", tid, tid, qtE);

    sem_post(&em);
}

void *leitor (void *threadid) {
    int *tid = (int*) threadid;

    printf("[L%d] Leitor %d inicializado\n", *tid, *tid);
  
    while(encerrarExecucao == 0) {
        entraLeitor(*tid);

        printf("[L%d] Leitor %d leitura: Count: %d, tId: %d\n", *tid, *tid, dados->count, dados->tId);
        

        usleep(DELAY_LEITURA_US);
        saiLeitor(*tid);

        usleep(DELAY_INTERVALO_US);
    }

    pthread_exit(NULL);
}

void *escritor (void *threadid) {
    int *tid = (int*) threadid;
    printf("[E%d] Escritor %d inicializado\n", *tid, *tid);
  
    while(encerrarExecucao == 0) {
        entraEscritor(*tid);
        dados->count++;
        dados->tId = *tid;

        usleep(DELAY_ESCRITA_US);
        saiEscritor(*tid);

        usleep(DELAY_INTERVALO_US);
    }

    pthread_exit(NULL);
}

// Interromper a execução após o timelimit
void *monitorExecucao (void *threadid) {
    usleep(TIMEOUT_US);

    encerrarExecucao = 1;

    pthread_exit(NULL);
}

//Função principal
int main(int argc, char *argv[]) {
    pthread_t tid_l[NLEITORES];
    pthread_t tid_e[NESCRITORES];
    pthread_t t_monitor;

    int *id_l[NLEITORES], *id_e[NESCRITORES], t;

    for (t=0; t<NLEITORES; t++) {
        if ((id_l[t] = (int*) malloc(sizeof(int))) == NULL) {
        pthread_exit(NULL); return 1;
        }
        *id_l[t] = t;

        tentativaLeituras[t] = 0;
        totalLeituras[t] = 0;
    }

    for (t=0; t<NESCRITORES; t++) {
        if ((id_e[t] = (int*) malloc(sizeof(int))) == NULL) {
        pthread_exit(NULL); return 1;
        }
        *id_e[t] = t;

        tentativaEscritas[t] = 0;
        totalEscritas[t] = 0;
    }

    dados = (t_dados *) malloc(sizeof(t_dados));
    dados->count = 0;
    sem_init(&em, 0, 1);
    
    qtL = qtE = 0;
    escritorQuerEntrar = 0;

    encerrarExecucao = 0;

    //Inicializa as threads
    for (t=0; t<NLEITORES; t++) {
        if (pthread_create(&tid_l[t], NULL, leitor, (void *)id_l[t] )) { printf("--ERRO: pthread_create()\n"); exit(-1); }
    }


    for (t=0; t<NESCRITORES; t++) {
        if (pthread_create(&tid_e[t], NULL, escritor, (void *)id_e[t] )) { printf("--ERRO: pthread_create()\n"); exit(-1); }
    }

    if (pthread_create(&t_monitor, NULL, monitorExecucao, 0)) { printf("--ERRO: pthread_create()\n"); exit(-1); }

    for (t=0; t<NLEITORES; t++) {
        if (pthread_join(tid_l[t], NULL)) {
            printf("--ERRO: pthread_join() \n"); exit(-1); 
        } 
    }

    for (t=0; t<NESCRITORES; t++) {
        if (pthread_join(tid_e[t], NULL)) {
            printf("--ERRO: pthread_join() \n"); exit(-1); 
        } 
    }

    printf("\n\n\nResultados (Núm leitor/escritor, porcentagem de sucesso): \n\nLeitores: \n\n");

    for(int i = 0; i < NLEITORES; i++) {
        printf("(L%d, %f)\n", i,  ((float) totalLeituras[i] / tentativaLeituras[i] * 100));
    }
    
    printf("\n\nEscritores: \n\n");

    for(int i = 0; i < NESCRITORES; i++) {
        printf("(E%d, %f)\n", i,  ((float) totalEscritas[i] / tentativaEscritas[i] * 100));
    }

    pthread_exit(NULL);
}