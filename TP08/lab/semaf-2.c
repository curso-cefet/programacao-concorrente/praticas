#include <stdio.h>
#include <stdlib.h> 
#include <pthread.h>
#include <semaphore.h>

#define NTHREADS 3


int x = 0;      

//semáforos para sincronizar a ordem de execução das threads
sem_t condt2, condt3;     


//Função executada pela thread 1
void *t1 (void *threadid) {
  int *tid = (int*) threadid;
  printf("Thread : %d esta executando...\n", *tid);
  
  x = 1;

  //permite que T2 execute
  sem_post(&condt2); 

  printf("Thread : %d terminou!\n", *tid);
  pthread_exit(NULL);
}

//Função executada pela thread 2
void *t2 (void *threadid) {
  int *tid = (int*) threadid;
  printf("Thread : %d esta executando...\n", *tid);

  //espera T1 executar
  sem_wait(&condt2); 

  x = 2;
  
  //permite que T3 execute
  sem_post(&condt3); 
  
  printf("Thread : %d terminou!\n", *tid);
  pthread_exit(NULL);
}

//Função executada pela thread 3
void *t3 (void *threadid) {
  int y;
  int *tid = (int*) threadid;
  printf("Thread : %d esta executando...\n", *tid);

  //espera T2 executar
  sem_wait(&condt3); 
  
  y = x;

  printf("Valor de y = %d\n", y);
  printf("Thread : %d terminou!\n", *tid);
  pthread_exit(NULL);
}

//Função principal
int main(int argc, char *argv[]) {
  pthread_t tid[NTHREADS];
  int *id[3], t;



  for (t=0; t<NTHREADS; t++) {
    if ((id[t] = (int*) malloc(sizeof(int))) == NULL) {
       pthread_exit(NULL); return 1;
    }
    *id[t] = t+1;
  }

  //Inicia os semáforos
  sem_init(&condt2, 0, 0);
  sem_init(&condt3, 0, 0);
  

  //Inicializa as threads
  if (pthread_create(&tid[2], NULL, t3, (void *)id[2])) { printf("--ERRO: pthread_create()\n"); exit(-1); }
  if (pthread_create(&tid[1], NULL, t2, (void *)id[1])) { printf("--ERRO: pthread_create()\n"); exit(-1); }
  if (pthread_create(&tid[0], NULL, t1, (void *)id[0])) { printf("--ERRO: pthread_create()\n"); exit(-1); }



  for (t=0; t<NTHREADS; t++) {
    if (pthread_join(tid[t], NULL)) {
         printf("--ERRO: pthread_join() \n"); exit(-1); 
    } 
    free(id[t]);
  }

  pthread_exit(NULL);
}
