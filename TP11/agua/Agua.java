import java.util.ArrayList;
import java.util.Vector;


class Agua {
    ArrayList<CasoTeste> casosDeTeste = new ArrayList<CasoTeste>();

    public static void main(String args[]) throws InterruptedException {
        Agua a = new Agua();
        a.executaTestes();
        a.imprimeTestes();
    }

    // Declara os casos de testes
    private void executaTestes() throws InterruptedException {
        this.novoCasoTeste(3);
        this.novoCasoTeste(10);
        this.novoCasoTeste(20);
        this.novoCasoTeste(50);
        this.novoCasoTeste(100);
    }

    // Executa os casos de testes
    private void novoCasoTeste(int numMoleculas) throws InterruptedException {
        CasoTeste casoTeste = new CasoTeste();
        this.casosDeTeste.add(casoTeste);
        casoTeste.numMoleculas = numMoleculas;

        casoTeste.executa();
    }

    // Imprime os dados para o gráfico final
    private void imprimeTestes() {
        StringBuilder plots = new StringBuilder();
        StringBuilder legendas = new StringBuilder();

        plots.append("Plots: ");
        legendas.append("Legendas: \n\n").append("\\legend {");

        String separator = "";
        for (CasoTeste result : this.casosDeTeste) {
            plots.append("\\addplot[color=red,mark=*, line width=0.2pt] coordinates { \n").append("\t(H, ")
                    .append(result.tempoHidrogenios).append(")\n").append("\t(O, ").append(result.tempoOxigenios)
                    .append(")\n").append("};");

            legendas.append(separator).append("\n").append("\t").append(result.numMoleculas).append(" moléculas");
            separator = ",";
        }

        legendas.append("\n}");

        System.out.println(plots.toString());
        System.out.println("\n--------------\n");
        System.out.println(legendas.toString());
    }

}
