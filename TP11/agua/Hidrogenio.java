import java.util.ArrayList;
import java.util.Vector;

public class Hidrogenio extends Atomo {

    Oxigenio o;
    
    public Hidrogenio(CasoTeste casoTeste, int id) {
        super(casoTeste, id);
    }


    @Override
    public void run() {
        if (super.logAtivo)
            System.out.println(">> Hidrogênio [" + id + "]: inicializado");

        // Átomo é adicionado à lista de hidrogênios com ligações pendentes
        super.casoTeste.hidrogeniosAvulsos.add(this);

        // Registra tempo de início
        super.initTimeMs = System.currentTimeMillis();

        try {
            /**
             * Método que procura por oxigênio disponível.
             * O método finaliza a execução quando a ligação é efetivada 
             */
            this.procuraOxigenio();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // Registra tempo em que o átomo concluiu a ligação com oxigênio
        super.finishTimeMs = System.currentTimeMillis();

        if (super.logAtivo)
            System.out.println(">> Hidrogênio [" + id + "]: finalizado");
    }

    /**
     * Faz procura por oxigênio disponível
     */
    private void procuraOxigenio() throws InterruptedException {
        // Flag que sinaliza se a ligação foi realizada com sucesso
        boolean pronto = false;

        // Segura a execução enquanto a ligação não foi realizada
        while (!pronto) {
            /**
             * Verifica, se, em algum momento da execução, algum oxigênio já
             * encontrou esse hidrogênio e concluiu a ligação.
             * 
             * É uma situação completamente possível, devido à preempção das threads
             */

            synchronized (this) {
                if (this.o != null) {

                    if (super.logAtivo)
                        System.out.println(
                                "<<<< >>>> Hidrogênio [" + id + "]: uma ligação foi feita pelo oxigênio [" + o.id + "]");
                    pronto = true;
                    break;
                }
            }


            /**
             * Verifica a lista de oxigênios com ligação pendente
             * 
             * Caso não haja oxigênios disponíveis, esse hidrogênio deverá aguardar (barreira)
             */
            if (super.casoTeste.oxigeniosAvulsos.size() == 0) {

                synchronized (this) {
                    if (super.logAtivo)
                        System.out.println("> Hidrogênio [" + id + "]: está dormindo");

                    this.wait();

                    if (super.logAtivo)
                        System.out.println(">>>> Hidrogênio [" + id + "]: acordou");
                }
            } else {

                synchronized (super.casoTeste) {

                    /**
                     * É necessário verificar novamente essas condições, pois 
                     * as verificações anteriores não foram realizadas dentro de uma seção
                     * crítica associada ao caso de teste, e com isso, nesse meio tempo
                     * algum átomo de oxigênio pode ter sido retirado da lista de átomos 
                     * disponíveis ou ter realizado ligação com esse hidrogênio
                     */
                    if (super.casoTeste.oxigeniosAvulsos.size() > 0 && this.o == null) {
                        if (super.logAtivo)
                            System.out.println("< > Hidrogênio [" + this.id + "]: Oxigenios avulsos: "
                                    + super.casoTeste.oxigeniosAvulsos.toString());

                        Oxigenio o = super.casoTeste.oxigeniosAvulsos.firstElement();

                        
                        synchronized (o) {
                            o.fazLigacao(this);
                            this.fazLigacao(o);

                            // Notifica o oxigênio, para que perceba que a ligação foi realizada
                            o.notifyAll();

                            pronto = true;
                            if (super.logAtivo)
                                System.out.println(
                                        "<<<< >>>> Hidrogênio [" + id + "]: ligação feita com oxigênio [" + o.id + "]");
                        }
                    }
                }
            }

        }

    }

    /**
     * Registro da ligação com um oxigêno
     */
    public synchronized void fazLigacao(Oxigenio o) {
        if (this.o == null) {
            this.o = o;

            /**
             * Remover da lista de hidrogênios avulsos, pois agora
             * a ligação com oxigênio foi realizada
             */
            super.casoTeste.hidrogeniosAvulsos.remove(this);
        }
    }

    @Override
    public String toString() {
        return "Hidrogênio [id" + id + "]";
    }

}
