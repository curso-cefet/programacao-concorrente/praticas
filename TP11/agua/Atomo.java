
class Atomo extends Thread {
    CasoTeste casoTeste;
    int id;

    // Momento em que foi inicializado
    long initTimeMs;

    // Momento em que o átomo finalizou as ligações
    long finishTimeMs;

    boolean logAtivo;

    public Atomo(CasoTeste casoTeste, int id) {
        this.casoTeste = casoTeste;
        this.id = id;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((casoTeste == null) ? 0 : casoTeste.hashCode());
        result = prime * result + id;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Atomo other = (Atomo) obj;
        if (casoTeste == null) {
            if (other.casoTeste != null)
                return false;
        } else if (!casoTeste.equals(other.casoTeste))
            return false;
        if (id != other.id)
            return false;
        return true;
    }

    

}