import java.util.ArrayList;
import java.util.Vector;

public class Oxigenio extends Atomo {

    Hidrogenio h1, h2;

    public Oxigenio(CasoTeste casoTeste, int id) {
        super(casoTeste, id);
    }

    @Override
    public void run() {
        if (super.logAtivo)
            System.out.println(">> Oxigênio [" + id + "]: inicializado");
        
        // Átomo é adicionado à lista de oxigênios com ligações pendentes
        super.casoTeste.oxigeniosAvulsos.add(this);

        // Registra tempo de início
        super.initTimeMs = System.currentTimeMillis();

        try {
            /**
             * Procura por dois hidrogênios disponível.
             * Os métodos finalizam as execuções quandos as ligações são efetivadas 
             */
            this.procuraHidrogenio();
            this.procuraHidrogenio();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // Registra tempo em que o átomo concluiu a ligação com os hidrogênios
        super.finishTimeMs = System.currentTimeMillis();

        if (super.logAtivo)
            System.out.println(">> Oxigênio [" + id + "]: finalizado");
    }

    /**
     * Faz procura por hidrogênio disponível
     */
    private void procuraHidrogenio() throws InterruptedException {
        // Flag que sinaliza se a ligação foi realizada com sucesso
        boolean pronto = false;

        // Segura a execução enquanto a ligação não foi realizada
        while (!pronto) {
            /**
             * Verifica, se, em algum momento da execução, algum hidrogênio já
             * encontrou esse oxigênio e concluiu as ligações.
             * 
             * É uma situação completamente possível, devido à preempção das threads
             */
            synchronized (this) {
                if (this.h1 != null && this.h2 != null) {

                    if (super.logAtivo)
                        System.out.println("<<<< >>>> Oxigênio [" + id + "]: ligações foram feitas pelo hidrogênios ["
                                + this.h1.id + "] e [" + this.h2.id + "]");
                    pronto = true;

                    break;
                }

            }

            /**
             * Verifica a lista de hidrogênios com ligação pendente
             * 
             * Caso não haja hidrogênios disponíveis, esse oxigênio deverá aguardar (barreira)
             */
            if (super.casoTeste.hidrogeniosAvulsos.size() == 0) {

                synchronized (this) {
                    if (super.logAtivo)
                        System.out.println("> Oxigênio [" + id + "]: está dormindo");

                    this.wait();

                    if (super.logAtivo)
                        System.out.println(">>>> Oxigênio [" + id + "]: acordou");
                }
            } else {

                synchronized (super.casoTeste) {

                    /**
                     * É necessário verificar novamente essas condições, pois 
                     * as verificações anteriores não foram realizadas dentro de uma seção
                     * crítica associada ao caso de teste, e com isso, nesse meio tempo
                     * algum átomo de hidrogênio pode ter sido retirado da lista de átomos 
                     * disponíveis ou ter realizado ligação com esse oxigênio
                     */
                    if (super.casoTeste.hidrogeniosAvulsos.size() > 0 && (this.h1 == null || this.h2 == null)) {
                        if (super.logAtivo)
                            System.out.println("< > Oxigênio [" + this.id + "]: Hidrogênios avulsos: "
                                    + super.casoTeste.hidrogeniosAvulsos.toString());

                        Hidrogenio h = super.casoTeste.hidrogeniosAvulsos.firstElement();

                        synchronized (h) {
                            h.fazLigacao(this);
                            this.fazLigacao(h);

                            // Notifica o hidrogênio, para que perceba que a ligação foi realizada
                            h.notifyAll();

                            pronto = true;
                            if (super.logAtivo)
                                System.out.println(
                                        "<<<< >>>> Oxigênio [" + id + "]: ligação feita com hidrogênio [" + h.id + "]");

                        }
                    }
                }
            }
        }
    }

    /**
     * Registro da ligação com um hidrogênio
     */
    public void fazLigacao(Hidrogenio h) {

        synchronized (h) {
            if (this.h1 == null) {
                this.h1 = h;
            } else {

                /**
                 * Remover da lista de oxigênios avulsos, pois agora
                 * a ligação com dois hidrogênios foram realizadas
                 */
                this.h2 = h;
                super.casoTeste.oxigeniosAvulsos.remove(this);
            }
        }
    }

    @Override
    public String toString() {
        return "Oxigenio [" + id + "]";
    }

}