import java.util.ArrayList;
import java.util.Vector;

public class CasoTeste {
    ArrayList<Hidrogenio> hidrogenios;
    ArrayList<Oxigenio> oxigenios;

    /**
     * Foram usadas estruturas do tipo Vector porque já
     * são sincronizadas para threads
     */
    Vector<Hidrogenio> hidrogeniosAvulsos;
    Vector<Oxigenio> oxigeniosAvulsos;

    int numMoleculas;
    double tempoHidrogenios;
    double tempoOxigenios;

    final int NUM_TESTES = 20;

    public void executa() throws InterruptedException {
        System.out.println("\n\n------------------------------");
        System.out.println("Caso de teste com " + this.numMoleculas + " moléculas inicializado");
        System.out.println("------------------------------\n\n");

        long somaHidrogenio = 0, somaOxigenio = 0;

        // Executa ${NUM_TESTES} vezes o mesmo caso de teste, para obter uma média geral
        for (int i = 0; i < NUM_TESTES; i++) {
            //System.out.println("repetição " + i);
            
            // Executa um teste
            this.testeParcial(false);

            // Somatório para cálculo o tempo médio em que hidrogênios
            // demoraram para integrar-se a uma molécula
            for (Hidrogenio h : this.hidrogenios) {
                somaHidrogenio += (h.finishTimeMs - h.initTimeMs);
            }

            // Somatório para cálculo o tempo médio em que oxigênios
            // demoraram para integrar-se a uma molécula
            for (Oxigenio o : this.oxigenios) {
                somaOxigenio += (o.finishTimeMs - o.initTimeMs);
            }
        }

        this.tempoHidrogenios = ((double) somaHidrogenio) /  ( NUM_TESTES * this.hidrogenios.size());
        this.tempoOxigenios = ((double) somaOxigenio) /  ( NUM_TESTES * this.oxigenios.size());

        System.out.println("\n\n------------------------------");
        System.out.println("Caso de teste com " + this.numMoleculas + " moléculas finalizado");
        System.out.println("------------------------------\n\n");

    }


    /**
     * Executa uma vez o teste de acordo com número de moléculas especficado
     * em ${this.numMoleculas}.
     * 
     * @param logAtivo: Se a execução deverá imprimir logs de execução.
     * Os logs mostram eventos como:
     *    - Átomo inicializado      
     *    - Átomo está esperando/dormingo
     *    - Átomo foi despertado / acordou
     *    - Átomo realizou ligação
     */
    private void testeParcial(boolean logAtivo) throws InterruptedException {

        int numH = 2 * this.numMoleculas;
        int numO = 1 * this.numMoleculas;

        this.hidrogenios = new ArrayList<Hidrogenio>(numH);
        this.hidrogeniosAvulsos = new Vector<Hidrogenio>(numH);

        this.oxigenios = new ArrayList<Oxigenio>(numO);
        this.oxigeniosAvulsos = new Vector<Oxigenio>(numO);


        /**
         * Inicializa os oxigênios e hidrogênios
         * 
         * Foram criadas duas threads separadas para inicialização de cada
         * tipo de elemento, para que a inicialização ocorra de forma mais
         * heterogênea possível
         */
        Thread thIniciaOxigenios, thIniciaHidrogenios;

        thIniciaHidrogenios = new Thread(() -> {
            for (int i = 0; i < numH; i++) {
                Hidrogenio h = new Hidrogenio(this, i);
                this.hidrogenios.add(h);
                h.logAtivo = logAtivo;
                h.start();
            }
        });

        thIniciaOxigenios = new Thread(() -> {

            for (int i = 0; i < numO; i++) {
                Oxigenio o = new Oxigenio(this, i);
                this.oxigenios.add(o);
                o.logAtivo = logAtivo;
                o.start();
            }
        });

        thIniciaHidrogenios.start();
        thIniciaOxigenios.start();

        // Aguarda pela inicialização de todos oxigênios e hidrogênios
        thIniciaHidrogenios.join();
        thIniciaOxigenios.join();

        try {
            /**
             * Aguarda a finalização de cada elemento
             * A finalização ocorre quando todos elementos estão associados em moléculas
             */
            for (int i = 0; i < numH; i++) {
                this.hidrogenios.get(i).join();
            }

            for (int i = 0; i < numO; i++) {
                this.oxigenios.get(i).join();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (logAtivo) {
            System.out.println("\n\nLigações: \n");

            for (Oxigenio o : this.oxigenios) {
                System.out.println("Oxigênio [" + o.id + "]: H[" + o.h1.id + "] H[" + o.h2.id + "]");
            }
        }
    }

}