import java.nio.channels.InterruptedByTimeoutException;

public class Pessoa extends Thread {
    CasoTeste casoTeste;
    int id;

    boolean homem;

    // Momento em que foi inicializado
    long initTimeMs;

    // Momento em que o átomo estava estável
    long finishTimeMs;

    boolean logAtivo;

    public Pessoa(CasoTeste casoTeste, int id, boolean homem) {
        this.casoTeste = casoTeste;
        this.id = id;
        this.homem = homem;
    }

    @Override
    public void run() {
        // Registra tempo de início
        this.initTimeMs = System.currentTimeMillis();

        try {
            if (this.homem) {
                this.casoTeste.monitor.entraHomem(this.id);

                // Registra tempo em que a pessoa esperou para entrar no banheiro
                this.finishTimeMs = System.currentTimeMillis();

                Thread.sleep(this.casoTeste.tempoPermanenciaBanheiro);
                this.casoTeste.monitor.saiHomem(this.id);
            } else {
                this.casoTeste.monitor.entraMulher(this.id);

                // Registra tempo em que a pessoa esperou para entrar no banheiro
                this.finishTimeMs = System.currentTimeMillis();

                Thread.sleep(this.casoTeste.tempoPermanenciaBanheiro);
                this.casoTeste.monitor.saiMulher(this.id);
            }

        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((casoTeste == null) ? 0 : casoTeste.hashCode());
        result = prime * result + id;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Atomo other = (Atomo) obj;
        if (casoTeste == null) {
            if (other.casoTeste != null)
                return false;
        } else if (!casoTeste.equals(other.casoTeste))
            return false;
        if (id != other.id)
            return false;
        return true;
    }

}