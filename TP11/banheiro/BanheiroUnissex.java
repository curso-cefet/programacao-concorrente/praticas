import java.util.ArrayList;
import java.util.Vector;
import java.lang.InterruptedException;

public class BanheiroUnissex {
    ArrayList<CasoTeste> casosDeTeste = new ArrayList<CasoTeste>();

    public static void main(String args[]) throws InterruptedException {
        BanheiroUnissex b = new BanheiroUnissex();
        b.executaTestes();
        b.imprimeTestes();
    }


    private void executaTestes() throws InterruptedException {
        System.out.println("testes");

        this.novoCasoTeste(5, 5, 90, 100, 100);
        this.novoCasoTeste(20, 20, 90, 100, 100);
        this.novoCasoTeste(5, 5, 120, 100, 100);
        this.novoCasoTeste(20, 20, 120, 100, 100);
    }

    private void novoCasoTeste(int numHomens, int numMulheres, long tempoPermanenciaBanheiro, long  intervaloChegadaMulheres, long intervaloChegadaHomens) throws InterruptedException {
        CasoTeste casoTeste = new CasoTeste();
        this.casosDeTeste.add(casoTeste);
        casoTeste.numHomens = numHomens;
        casoTeste.numMulheres = numMulheres;
        casoTeste.tempoPermanenciaBanheiro = tempoPermanenciaBanheiro;
        casoTeste.intervaloChegadaHomens = intervaloChegadaHomens;
        casoTeste.intervaloChegadaMulheres = intervaloChegadaMulheres;

        casoTeste.executa();
    }

    private void imprimeTestes() {
        StringBuilder plots = new StringBuilder();
        StringBuilder legendas = new StringBuilder();

        plots.append("Plots: ");
        legendas.append("Legendas: \n\n").append("\\legend {");

        String separator = "";
        for (CasoTeste result : this.casosDeTeste) {
            plots.append("\\addplot[color=red,mark=*, line width=0.2pt] coordinates { \n").append("\t(H, ")
                    .append(result.tempoMedioHomens).append(")\n").append("\t(M, ").append(result.tempoMedioMulheres)
                    .append(")\n").append("};");

            legendas.append(separator).append("\n").append("\t")
                    .append(result.numHomens).append(" homens e ").append(result.numMulheres).append(" mulheres. ")
                    .append("Tempo de permanência: " + result.tempoPermanenciaBanheiro + " ms");
            separator = ",";
        }

        legendas.append("\n}");

        System.out.println(plots.toString());
        System.out.println("\n--------------\n");
        System.out.println(legendas.toString());
    }
}
