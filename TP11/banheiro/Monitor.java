
public class Monitor {

    private int qtHomensDentro, qtMulheresDentro;
    private int qtHomensEspera, qtMulheresEspera;

    private char prioridade;

    private boolean logAtivo;

    public Monitor(boolean logAtivo) {
        this.logAtivo = logAtivo;

        this.qtHomensDentro = 0;
        this.qtMulheresDentro = 0;

        this.qtHomensEspera = 0;
        this.qtMulheresEspera = 0;

        this.prioridade = ' ';
    }

    public synchronized void entraHomem(int id) throws InterruptedException {
        if (this.logAtivo)
            System.out.println("-- Homem [" + id + "] tenta entrar no banheiro");

        while (this.qtMulheresDentro > 0 || this.prioridade == 'm') {
            // Há mulheres no banheiro
            if (this.logAtivo) {

                if (this.prioridade == 'm') {
                    System.out.println("-- Homem [" + id + "] precisa esperar. Mulheres tem prioridade");
                } else {
                    System.out.println("-- Homem [" + id + "] precisa esperar. Há mulheres no banheiro");
                }
            }

            if (this.qtMulheresEspera == 0 && this.prioridade == ' ') {
                // Não há mulheres esperando, homens podem ter prioridadeif(this.logAtivo)

                if (this.logAtivo)
                    System.out.println("-- Homens ganham prioridade de entrada");

                this.prioridade = 'h';
            }

            // Deve esperar
            this.qtHomensEspera++;
            this.wait();
            this.qtHomensEspera--;

            if (this.logAtivo)
                System.out.println("-- Homem [" + id + "] despertou");
        }

        if (this.logAtivo)
            System.out.println("-- Homem [" + id + "] entrou. " + (this.qtHomensDentro + 1) + " homens no banheiro");

        if (this.prioridade == 'h' && this.qtHomensEspera == 0) {
            // A prioridade era dos homens, mas todos já entraram

            if (this.qtMulheresEspera > 0) {
                // Há mulheres em espera

                if (this.logAtivo)
                    System.out.println("-- Não há mais homens em espera. Prioridade atribuida às mulheres");

                this.prioridade = 'm';
            } else {
                if (this.logAtivo)
                    System.out.println("-- Não há mais homens nem mulheres em espera. Prioridade resetada");

                this.prioridade = ' ';
            }
        }

        // Nesse ponto, o homem pode entrar
        this.qtHomensDentro++;
    }

    public synchronized void saiHomem(int id) throws InterruptedException {
        this.qtHomensDentro--;

        if (this.logAtivo)
            System.out.println("-- Homem [" + id + "] saiu");

        // Notifica as threads
        notifyAll();
    }

    public synchronized void entraMulher(int id) throws InterruptedException {
        if (this.logAtivo)
            System.out.println("-- Mulher [" + id + "] tenta entrar no banheiro");

        while (this.qtHomensDentro > 0 || this.prioridade == 'h') {
            // Há homens no banheiro
            if (this.logAtivo) {
                if (this.prioridade == 'h') {
                    System.out.println("-- Mulher [" + id + "] precisa esperar. Homens tem prioridade");
                } else {
                    System.out.println("-- Mulher [" + id + "] precisa esperar. Há homens no banheiro");
                }
            }

            if (this.qtHomensEspera == 0 && this.prioridade == ' ') {
                // Não há homens esperando, mulheres podem ter prioridade

                if (this.logAtivo)
                    System.out.println("-- Mulheres ganham prioridade de entrada");

                this.prioridade = 'm';
            }

            // Deve esperar
            this.qtMulheresEspera++;
            this.wait();
            this.qtMulheresEspera--;

            if (this.logAtivo)
                System.out.println("-- Mulher [" + id + "] despertou");
        }

        if (this.logAtivo)
            System.out
                    .println("-- Mulher [" + id + "] entrou. " + (this.qtMulheresDentro + 1) + " mulheres no banheiro");

        if (this.prioridade == 'm' && this.qtMulheresEspera == 0) {
            // A prioridade era das mulheres, mas todas já entraram

            if (this.qtHomensEspera > 0) {
                // Há homens em espera

                if (this.logAtivo)
                    System.out.println("-- Não há mais mulheres em espera. Prioridade atribuida aos homens");

                this.prioridade = 'h';
            } else {
                if (this.logAtivo)
                    System.out.println("-- Não há mais homens nem mulheres em espera. Prioridade resetada");

                this.prioridade = ' ';
            }
        }

        // Nesse ponto, a mulher pode entrar
        this.qtMulheresDentro++;
    }

    public synchronized void saiMulher(int id) throws InterruptedException {

        this.qtMulheresDentro--;

        if (this.logAtivo)
            System.out.println("-- Mulher [" + id + "] saiu");

        // Notifica as threads
        notifyAll();
    }
}