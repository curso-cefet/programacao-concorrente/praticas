import java.util.ArrayList;
import java.util.Vector;

public class CasoTeste {
    ArrayList<Pessoa> homens;
    ArrayList<Pessoa> mulheres;

    int numHomens, numMulheres;
    long intervaloChegadaMulheres, intervaloChegadaHomens;
    long tempoPermanenciaBanheiro;

    double tempoMedioHomens;
    double tempoMedioMulheres;

    Monitor monitor;

    final int NUM_TESTES = 20;

    public void executa() throws InterruptedException {
        System.out.println("\n\n------------------------------");
        System.out.println(
                "Caso de teste com " + this.numHomens + " homens e " + this.numMulheres + " mulheres inicializado");
        System.out.println("------------------------------\n\n");

        long somaHomens = 0, somaMulheres = 0;

        // this.testeParcial(true);

        for (int i = 0; i < NUM_TESTES; i++) {
            System.out.println("repetição " + i);
            this.testeParcial(false);

            // Somatório para cálculo o tempo médio em que homens
            // esperaram para entrar no banheiro 
            for (Pessoa p : this.homens) {
                somaHomens += (p.finishTimeMs - p.initTimeMs);
            }

            // Somatório para cálculo o tempo médio em que mulheres
            // esperaram para entrar no banheiro 
            for (Pessoa p : this.mulheres) {
                somaMulheres += (p.finishTimeMs - p.initTimeMs);
            }
        }


        this.tempoMedioHomens = ((double) somaHomens) /  ( NUM_TESTES * this.homens.size());
        this.tempoMedioMulheres = ((double) somaMulheres) /  ( NUM_TESTES * this.mulheres.size());

        System.out.println("\n\n------------------------------");
        System.out.println(
                "Caso de teste com " + this.numHomens + " homens e " + this.numMulheres + " mulheres finalizado");
        System.out.println("------------------------------\n\n");

    }

    private void testeParcial(boolean logAtivo) throws InterruptedException {
        monitor = new Monitor(logAtivo);

        this.homens = new ArrayList<Pessoa>(this.numHomens);
        this.mulheres = new ArrayList<Pessoa>(this.numMulheres);

        // Inicializa os homens e mulheres

        Thread thIniciaHomens, thIniciaMulheres;

        thIniciaHomens = new Thread(() -> {
            for (int i = 0; i < numHomens; i++) {
                Pessoa p = new Pessoa(this, i, true);
                this.homens.add(p);
                p.logAtivo = logAtivo;
                p.start();

                try {
                    Thread.sleep(this.intervaloChegadaHomens);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        thIniciaMulheres = new Thread(() -> {

            for (int i = 0; i < numMulheres; i++) {
                Pessoa p = new Pessoa(this, i, false);
                this.mulheres.add(p);
                p.logAtivo = logAtivo;
                p.start();

                try {
                    Thread.sleep(this.intervaloChegadaMulheres);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        thIniciaHomens.start();
        thIniciaMulheres.start();

        thIniciaHomens.join();
        thIniciaMulheres.join();

        try {

            // Aguarda a finalização
            for (int i = 0; i < this.numHomens; i++) {
                this.homens.get(i).join();
            }

            for (int i = 0; i < this.numMulheres; i++) {
                this.mulheres.get(i).join();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}