#include <stdio.h>
#include <stdlib.h> 
#include <pthread.h>
#include <semaphore.h>

#define NITER 5
#define NTHREADS 4

int vet[NTHREADS];

// Exclusão mútua
sem_t em ;

// Semáforo binário que autoriza ou bloqueia 
// a execução de uma thread
sem_t continua [ NTHREADS ];


// Quantidade de threads que ainda não chegaram na barreira
int cont;


void barreira(int *tid, int aux) {
    int t;


    sem_wait (&em); 

    cont--;
    if ( cont == 0) {
        cont = NTHREADS ;

        printf("Thread %d: Todas threads atingiram a barreira %d\n", *tid, aux);


        for ( t =0; t < NTHREADS ; t ++) {
            // A thread atual não está bloqueada, logo não pode receber um sem_post
            if( (t+1) != *tid) {
                printf("Thread %d vai liberar thread %d\n", *tid, (t+1));
                sem_post(&continua[ t ]);
                printf("Thread %d liberou thread %d\n", *tid, (t+1));
            }
        }
    } else {
        // Necessário liberar a exclusão mútua, para que outra thread possa adentrá-la
        sem_post(&em);

        printf("Thread %d chega na barreira %d e precisa esperar\n", *tid, aux);
        sem_wait(&continua[*tid - 1]) ;
        
        // Retoma a exclusão mútua
        sem_wait(&em);
    }
    sem_post (&em);
}

void *thread( void *threadid) {
    int *tid = (int*) threadid;
    int aux = 1;

    vet[*tid] = *tid;

    printf("Thread %d inicializada\n", *tid);
    printf("Thread %d chegou na barreira %d\n", *tid, aux);

    // espera coletiva
    barreira(tid, aux);


    printf("Thread %d saiu da barreira %d\n", *tid, aux);
    aux++;

    for(int i = 0;  i < NITER; i++) {
        vet [* tid ] = vet [* tid ] * vet [* tid ] + vet [* tid -1];
        printf ("Thread %d chegou na barreira % d \n " , * tid , aux ) ;
        
        // espera coletiva
        barreira (tid, aux) ; 
        
        printf ("Thread %d saiu da barreira % d \n " , * tid , aux );
        aux ++;
    }


    printf("Thread %d finalizada\n", *tid);
    
    free ( tid ) ;
    pthread_exit(NULL);
}


//Função principal
int main(int argc, char *argv[]) {
    pthread_t tid[NTHREADS];
    int *id[NTHREADS], t;


    for (t=0; t<NTHREADS; t++) {
        if ((id[t] = (int*) malloc(sizeof(int))) == NULL) {
        pthread_exit(NULL); return 1;
        }
        *id[t] = t+1;
    }

    // Inicializa semáforos

    sem_init (& em , 0 , 1) ;
    for ( t =0; t < NTHREADS ; t ++) {
        /**
         * Semáforo que impõe condição na qual as threads podem 
         * transpor a barreira 
         */
        sem_init (&continua [t] , 0 , 0) ;
    }
    

    // Quantidade de threads que ainda não chegaram na barreira
    cont = NTHREADS;



    for ( t =0; t < NTHREADS ; t ++) {
        if (pthread_create(&tid[t], NULL, thread, (void *)id[t])) { 
            printf("--ERRO: pthread_create()\n"); exit(-1); 
        }
    }

    for (t=0; t<NTHREADS; t++) {
        if (pthread_join(tid[t], NULL)) {
            printf("--ERRO: pthread_join() \n"); exit(-1); 
        } 
        free(id[t]);
    }


    pthread_exit(NULL);
}
