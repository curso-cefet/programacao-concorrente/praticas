
/* Disciplina: Computacao Concorrente */
/* Codigo: Criando um pool de threads em Java */

import java.util.LinkedList;

//-------------------------------------------------------------------------------
class FilaTarefas {
    // número de threads utilizadas
    private final int nThreads;

    // Threads disponíveis para uso
    private final MyPoolThreads[] threads;

    // Fila de tarefas
    private final LinkedList<Runnable> queue;

    // Comando de finalização acionado
    private boolean shutdown;

    public FilaTarefas(int nThreads) {
        this.shutdown = false;
        this.nThreads = nThreads;
        queue = new LinkedList<Runnable>();
        threads = new MyPoolThreads[nThreads];

        // Inicializa as threads
        for (int i = 0; i < nThreads; i++) {
            threads[i] = new MyPoolThreads(i + 1);
            threads[i].start();
        }
    }

    // Recebe um objeto executável
    public void execute(Runnable r) {

        synchronized (queue) {
            // Impede que novas tarefas sejam adicionadas
            // caso o pool tenha recebido comando de
            // finalização
            if (this.shutdown)
                return;

            // Adiciona o executável à fila
            queue.addLast(r);

            // Notifica uma thread que esteja dormindo
            queue.notify();

        }
    }

    // Comando de finalização do pool
    public void shutdown() {
        synchronized (queue) {
            this.shutdown = true;

            // Acorda as threads que estão dormindo,
            // para que encerrem a atividade
            queue.notifyAll();
        }

        // Aguarda a finalização de todas threads
        for (int i = 0; i < nThreads; i++) {
            try {
                threads[i].join();
            } catch (InterruptedException e) {
                return;
            }
        }
    }

    private class MyPoolThreads extends Thread {
        private int id;

        public MyPoolThreads(int id) {
            this.id = id;
        }

        public void run() {
            Runnable r;

            // Laço de repetição indeterminado
            while (true) {

                // Sincronização. Impede que duas
                // threads manipulem a fila e causem
                // condição de corrida
                synchronized (queue) {

                    // Laço que mantém a thread em inatividade
                    // caso não haja tarefas na fila e
                    // o comando de finalização não tenha sido
                    // acionado
                    while (queue.isEmpty() && (!shutdown)) {
                        try {
                            // Mantém thread em inatividade
                            queue.wait();

                            // Nesse ponto, a thread foi despertada
                            // porque chegou uma nova atividade
                            // ou o comando de finalização foi
                            // acionado
                        } catch (InterruptedException ignored) {
                        }
                    }

                    // if (queue.isEmpty() && shutdown) return;

                    // Thread foi despertada pelo comando de finalização,
                    // e não há mais tarefas pendentes.
                    // Finaliza a execução
                    if (queue.isEmpty())
                        return;

                    r = (Runnable) queue.removeFirst();
                }
                try {
                    // Executa a tarefa
                    System.out.println(String.format("\nThread %d vai executar uma tarefa", this.id));
                    r.run();
                    System.out.println(String.format("Thread %d executou a tarefa", this.id));
                } catch (RuntimeException e) {
                }
            }

        }
    }
}
// -------------------------------------------------------------------------------

// --PASSO 1: cria uma classe que implementa a interface Runnable
class Hello implements Runnable {
    String msg;

    public Hello(String m) {
        msg = m;
    }

    // --metodo executado pela thread
    public void run() {
        System.out.println(msg);
    }
}

class Primo implements Runnable {
    int n;

    public Primo(int n) {
        this.n = n;
    }

    // recebe um numero inteiro positivo e imprime se
    // é primo ou nao
    public void run() {
        System.out.println(String.format("Número %d %sé primo", n, ehPrimo()? "" : "não "));
    }

    boolean ehPrimo() {
        int i;
        if (n <= 1)
            return false;
        if (n == 2)
            return true;
        if (n % 2 == 0)
            return false;
        for (i = 3; i < Math.sqrt(n) + 1; i += 2) {
            if (n % i == 0)
                return false;
        }
        return true;
    }
}

// Classe da aplicação (método main)
class MyPool {
    private static final int NTHREADS = 4;

    public static void main(String[] args) {
        // --PASSO 2: cria o pool de threads
        FilaTarefas pool = new FilaTarefas(NTHREADS);

        // --PASSO 3: dispara a execução dos objetos runnable usando o pool de threads
        for (int i = 0; i < 100; i++) {
            final String m = "Hello da tarefa " + i;
            Runnable hello = new Hello(m);
            pool.execute(hello);


            Runnable primo = new Primo(i);
            pool.execute(primo);
        }

        // --PASSO 4: esperar pelo termino das threads
        pool.shutdown();
        System.out.println("Terminou");
    }
}
