#include "stdafx.h"
#include <iostream>
#include <pthread.h>
#include <windows.h>
#include <winuser.h>

using namespace std;

#define SIZE  500

int main(int argc, char *argv[]) {

	HWND myconsole = CreateWindowA("STATIC", "Rembrandt", WS_VISIBLE, 0, 0, SIZE + 15, SIZE + 35, NULL, NULL, NULL, NULL); //nova janela

	//Get a handle to device context
	HDC mydc = GetDC(myconsole);

	//Draw pixels
	int r, g, b;
	for (int i = 0; i < SIZE; i++){
		for (int j = 0; j < SIZE; j++){

			r = (int)sqrt(pow(i - 0   , 2) + pow(j - 0   , 2));
			g = (int)sqrt(pow(i - 0   , 2) + pow(j - SIZE, 2));
			b = (int)sqrt(pow(i - SIZE, 2) + pow(j - SIZE, 2));
			 
			SetPixel(mydc, i, j, RGB(r, g, b));
		}
	}

	ReleaseDC(myconsole, mydc);

	getchar();
	return 1;
}



