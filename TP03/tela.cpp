// Compilação: g++ tela.cpp -o tela -lsfml-graphics -lsfml-window -lsfml-system -lpthread


#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <SFML/Graphics.hpp>
#include <iostream>
#include <math.h>
#include <unistd.h>
#include <chrono>

using namespace std;
using namespace sf;

#define SIZE 512

typedef struct {
    int idThread;
    int colMin;
    int colMax;
} tv_Args;

Image imagem;
Texture textura;
Sprite sprite;
RenderWindow janela(VideoMode(SIZE,SIZE), "minha janela");


tv_Args* getArgs(int idThread, int colunas_per_thread) {
    tv_Args *arg;
    
    arg = (tv_Args *) malloc(sizeof(tv_Args));
    if(arg == NULL) {
        cout << "--ERRO: malloc()\n";
        return NULL;
    }

    arg->idThread = idThread;
    arg->colMin = (idThread - 1) * colunas_per_thread;
    arg->colMax = arg->colMin + colunas_per_thread - 1;
    return arg;
}


void atualizaTela() {
    textura.loadFromImage(imagem);
    sprite.setTexture(textura);
    janela.draw(sprite);
    janela.display();
}

void *atualizaTelaThread(void *_arg) {
    while(1) {
        atualizaTela();
        usleep(10000);
    }
}


void *exibeImagemThread(void *_arg) {
    tv_Args *arg = (tv_Args *) _arg;



    int r, g, b;

    for(int i = arg->colMin ; i <= arg->colMax ; i++) {
        for(int j = 0 ; j < SIZE ; j++) {
            r = (Uint8)sqrt(pow(i - 0   , 2) + pow(j - 0   , 2));
            g = (Uint8)sqrt(pow(i - 0   , 2) + pow(j - SIZE, 2));
            b = (Uint8)sqrt(pow(i - SIZE, 2) + pow(j - SIZE, 2));
            imagem.setPixel(i,j,Color(r,g,b));
        }
        
        usleep(5000);
    }


    free(arg);
    pthread_exit(NULL);

    return 0;
}

/*
Segurar a execução do processo até que a janela seja fechada,
além de permitir o registro de outros eventos

void esperaJanela(RenderWindow* janela, Sprite* sprite) {
    Event event;
    while(janela->isOpen()) {
        while(janela->pollEvent(event)){
            if(event.type == Event::Closed) 
                janela->close();
            if(event.type == Event::Resized){
                janela->clear();
                janela->draw(*sprite);
                janela->display();
            }
        }
    }
}*/


int main(int argc,char* argv[]) {
    if(argc < 2) {
        cout << "--ERRO: Informe o número de threads\n\n";
        return -1;
    }

    int num_threads = atoi(argv[1]);

    imagem.create(SIZE,SIZE,Color(0,0,0));
    Uint8 r,g,b;

    pthread_t tid_vector[num_threads];

    int colunas_per_thread = SIZE / num_threads;
    tv_Args *arg;

    auto start = chrono::high_resolution_clock::now();

    for(int t = 0; t < num_threads; t++) {
        arg = getArgs(t+1, colunas_per_thread);

        if(arg == NULL)
            return -1;


        if(pthread_create(&tid_vector[t], NULL, exibeImagemThread, (void *) arg)) {
            cout << "--ERRO: pthread_create()\n";
            return -1;
        }
    }

    pthread_t threadAtualizarImagem;

    if(pthread_create(&threadAtualizarImagem, NULL, atualizaTelaThread, NULL)) {
        cout << "--ERRO: pthread_create()\n";
        return -1;
    }

    for(int t = 0; t < num_threads; t++) {
        if(pthread_join(tid_vector[t], NULL)) {
            cout << "--ERRO: pthread_join()\n";
        }
    }


    if(pthread_cancel(threadAtualizarImagem)) {
        cout << "--ERRO: pthread_cancel()\n";
        return -1;
    }

    atualizaTela();
    chrono::duration<double> tempo = chrono::high_resolution_clock::now() - start;

    cout << "\nTempo de processamento: " << tempo.count();

    //esperaJanela(&janela, &sprite);

    janela.close();
    
    cout << "\n";
    
    return 0;
}

