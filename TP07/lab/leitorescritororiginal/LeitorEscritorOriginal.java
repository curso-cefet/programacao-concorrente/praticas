package leitorescritororiginal;

public class LeitorEscritorOriginal {

    static final int L = 6;
    static final int E = 3;

    /**
     * testeNum
     * 
     * 1 = Atividade 1
     *      -São permitidas múltiplas leituras simultâneas
     *      -Não são permitidas múltiplas escritas simultâneas
     *      -Não são permitidas leituras e escritas simultâneas
     * 
     * 
     * 2 = Atividade 2
     *      -Operação de escrita dentro do monitor
     * 
     * 
     * 3 = Atividade 3
     *      -Prioridade de escrita. 
     *      sempre que um escritor está esperando para escrever, 
     *      novos leitores não podem começar a ler.
     */
    static final int testeNum = 3;

    static final int tempoMonitoramentoMs = 10000;

    public static void main(String[] args) {
        int i;
        Monitor monitor = new Monitor(L, E, testeNum == 3);  // Monitor (objeto compartilhado entre leitores e escritores)
        Leitor[] l = new Leitor[L];       // Threads leitores

        Escritor[] e = new Escritor[E];   // Threads escritores
        
        

        MonitorarExecucao m = new MonitorarExecucao(tempoMonitoramentoMs, monitor, l, e);
        m.start();

        
        for (i = 0; i < L; i++) {
            l[i] = new Leitor(i + 1, (i + 1) * 500, monitor);
            l[i].start();
        }
        for (i = 0; i < E; i++) {
            if(testeNum == 1)
                e[i] = new Escritor(i + 1, (i + 1) * 500, monitor);
            else if(testeNum == 2 || testeNum == 3)
                e[i] = new Escritor2(i + 1, (i + 1) * 500, monitor);

            e[i].start();
        }
    }
}

class MonitorarExecucao extends Thread {
    long milisTime;
    Monitor monitor;
    Leitor l[];
    Escritor e[];

    MonitorarExecucao(long milisTime, Monitor monitor, Leitor[] l, Escritor[] e) {
        this.milisTime = milisTime;
        this.monitor = monitor;

        this.l = l;
        this.e = e;
    }


    @Override
    public void run() {
        try {
            sleep(milisTime);
        } catch (InterruptedException e) {
        }
        

        for (int i = 0; i < LeitorEscritorOriginal.L; i++) {
            l[i].interrupt();
        }
        for (int i = 0; i <  LeitorEscritorOriginal.E; i++) {
            e[i].interrupt();
        }

        

        System.out.println("\n\n\nFinalizado\n\n");

        
        this.monitor.imprimeResultados();

        System.exit(0);
    }
}
