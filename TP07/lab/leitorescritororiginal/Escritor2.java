package leitorescritororiginal;

/**
 * Método escreve() completamente encapsulado no no monitor
 */
public class Escritor2 extends Escritor {

    Escritor2(int id, int delayTime, Monitor m) {
        super(id, delayTime, m);
    }

    @Override
    public void run() {
        try {
            for (;;) {
                this.monitor.escreve(this.id);
                sleep(this.delay); //atraso bobo...
            }
        } catch (InterruptedException e) {
        }
    }
}
