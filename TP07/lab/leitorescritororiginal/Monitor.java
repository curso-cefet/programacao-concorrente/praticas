package leitorescritororiginal;

public class Monitor {

    private int totalL, totalE;
    private int totalLeituras[], totalTentativasLeitura[];
    private int totalEscritas[], totalTentativasEscrita[];

    private long tempoEsperaLeitura[], tempoEsperaEscrita[];

    private boolean prioridadeEscrita;
    private int escritorQuerEntrar;
    

    private int leit, escr;

    // Construtor
    Monitor(int L, int E, boolean prioridadeEscrita) {
        this.prioridadeEscrita = prioridadeEscrita;
        this.escritorQuerEntrar = 0;

        this.leit = 0;
        this.escr = 0;

        this.totalL = L;
        this.totalE = E;

        this.totalLeituras = new int[L+1];
        this.totalTentativasLeitura = new int[L+1];
        this.tempoEsperaLeitura = new long[L+1];

        this.totalEscritas = new int[E+1];
        this.totalTentativasEscrita = new int[E+1];
        this.tempoEsperaEscrita = new long[L+1];
    }

    void imprimeResultados() {
        System.out.println("Leitores: \n\n");

        for(int i = 1; i <= totalL; i++) {
            System.out.println("Leitor " + i + "  -  Leituras sucedidas: " + 
                this.totalLeituras[i] + ". Tentativas: " + this.totalTentativasLeitura[i] + 
                ". Sucesso: " + ((double) this.totalLeituras[i] / this.totalTentativasLeitura[i] * 100) + "%" +
                ". Tempo de espera total (até obter sucesso): " + this.tempoEsperaLeitura[i]);
        }

        
        System.out.println("\n\nEscritores: \n\n");

        for(int i = 1; i <= totalE; i++) {
            System.out.println("Escritor " + i + "  -  Escritas sucedidas: " + 
                this.totalEscritas[i] + ". Tentativas: " + this.totalTentativasEscrita[i] + 
                ". Sucesso: " + ((double) this.totalEscritas[i] / this.totalTentativasEscrita[i] * 100) + "%" +
                ". Tempo de espera total (até obter sucesso): " + this.tempoEsperaEscrita[i]);
        }
    }

    // Entrada para leitores
    public synchronized void entraLeitor(int id) {
        try {
            long tStart = System.currentTimeMillis();
            boolean espera = false;



            while (this.escr > 0 || (this.prioridadeEscrita && this.escritorQuerEntrar > 0)) {
                espera = true;
                this.totalTentativasLeitura[id]++;

                if(!this.prioridadeEscrita)
                    System.out.println("L[" + id + "]: esta esperando");
                else
                    System.out.println("L[" + id + "]: esta esperando. " + this.escritorQuerEntrar + 
                        " escritores estão com prioridade de escrita");

                wait();  //bloqueia pela condicao logica da aplicacao 
            }

            this.totalTentativasLeitura[id]++;
            this.totalLeituras[id]++;

            this.leit++;
            
            if(espera) {
                long tempoEspera = (System.currentTimeMillis() - tStart) ;

                System.out.println("L[" + id + "]: entrou, total de " + this.leit + 
                        " lendo. Tempo de espera: " + tempoEspera + "ms");
                
                this.tempoEsperaLeitura[id] += tempoEspera;
            } else {
                System.out.println("L[" + id + "]: entrou, total de " + this.leit + " lendo.");
            }

        } catch (InterruptedException e) {
        }
    }

    // Saida para leitores
    public synchronized void saiLeitor(int id) {
        this.leit--;
        if (this.leit == 0) {
            notifyAll(); //libera os escritores (caso exista escritor bloqueado)
        }
        System.out.println("L[" + id + "]: saiu, resta(m) " + this.leit + " leitor(es) lendo");
    }

    // Entrada para escritores
    public synchronized void entraEscritor(int id) {
        try {
            long tStart = System.currentTimeMillis();
            boolean espera = false;

            while ((this.leit > 0) || (this.escr > 0)) {
                espera = true;
                this.totalTentativasEscrita[id]++;
                System.out.println("E[" + id + "]: esta esperando");
                this.escritorQuerEntrar++;
                wait();  //bloqueia pela condicao logica da aplicacao 
            }

            this.escritorQuerEntrar--;

            
            this.totalTentativasEscrita[id]++;
            this.totalEscritas[id]++;

            this.escr++;
            
            if(espera) {
                long tempoEspera = (System.currentTimeMillis() - tStart) ;

                System.out.println("E[" + id + "]: entrou, total de " + this.escr + 
                        " escrevendo. Tempo de espera: " + tempoEspera + "ms");
                        
                this.tempoEsperaEscrita[id] += tempoEspera;
            } else {
                System.out.println("E[" + id + "]: entrou, total de " + this.escr + " escrevendo"); //se aparecer mais de um escritor escrevendo, ERRO!
            }

        } catch (InterruptedException e) {
        }
    }

    // Saida para escritores
    public synchronized void saiEscritor(int id) {
        this.escr--;
        notifyAll(); //libera os leitores e outros escritores no caso de estarem bloqueados
        System.out.println("E[" + id + "]: saiu, resta(m) " + this.escr + " escritor(es) escrevendo"); //se aparecer mais de um escritor escrevendo, ERRO!
    }


    public synchronized void escreve(int id) {
        
        try {
            long tStart = System.currentTimeMillis();
            boolean espera = false;

            while ( (this.leit > 0) || (this.escr > 0) ) {
                espera = true;
                this.totalTentativasEscrita[id]++;
                System.out.println("E[" + id + "]: esta esperando");
                this.escritorQuerEntrar++;
                wait();  //bloqueia pela condicao logica da aplicacao 
            }

            
            this.escritorQuerEntrar--;
            
            this.totalTentativasEscrita[id]++;
            this.totalEscritas[id]++;

            this.escr++;
            
            System.out.println("E[" + id + "]: iniciou a escrita, total de " + this.escr + " escrevendo");

            long tempoEspera = (System.currentTimeMillis() - tStart) ;

            
            Thread.sleep(500); //...para simbolizar o tempo de escrita
            
            this.escr--;
            notifyAll(); 

            
            if(espera) {
                System.out.println("E[" + id + "]: escreveu, total de " + this.escr + 
                        " escrevendo. Tempo de espera: " + tempoEspera + "ms");
                        
                this.tempoEsperaEscrita[id] += tempoEspera;
            } else {
                System.out.println("E[" + id + "]: escreveu, total de " + this.escr + " escrevendo"); //se aparecer mais de um escritor escrevendo, ERRO!
            }

        } catch (InterruptedException e) {
        }
    }

    
}
