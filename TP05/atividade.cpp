#include <iostream>
#include <pthread.h>
#include <chrono>
#include <math.h>       /* log */

using namespace std;


pthread_mutex_t mutex; 

int **mat;
int n;
int num_threads;

void imprimeMatriz() {
    FILE *fsaida = fopen("saida-atividade", "w");


    if(fsaida == NULL) {
        perror ("Falha ao abrir arquivo de saída");
        return;
    } 


    for(int i = 0; i < n; i++) {
        for(int j = 0; j < n; j++) {
            fprintf(fsaida, "%d ", mat[i][j]);
        }


        fprintf(fsaida, "\n");
    }


    fclose(fsaida);

    cout << "\n\n";
}

void *ExecutaTarefa(void *threadid) {
    int *tid = (int*) threadid;

    //printf("Thread : %d esta executando...\n", *tid);

    for(int i = 0; i < n; i++) {
        for(int j = 0; j < n; j++) {
            pthread_mutex_lock(&mutex); 

            //printf("Thread : %d na célula(%d, %d)...\n", *tid, i, j);
            if(mat[i][j] == 0) {
                mat[i][j] += (1 << (*tid)); ;
                //cout << "\ntid: " << *tid << "   -   " << "mat[i][j] = " << (1 << (*tid)); 
            }
            
            pthread_mutex_unlock(&mutex);
        }
    }

    //printf("Thread : %d terminou!\n", *tid);
    free(threadid);
    pthread_exit(NULL);
    return 0;
}

// funcao/thread principal do programa
int main(int argc, char *argv[]) {

    if(argc < 3) {
        cout << "--ERRO: Argumentos inválidos. Uso: <dimensão N da matriz NxN> <número de threads>\n\n";
        return -1;
    }

    // Leitura do argumento de tamanho da matriz
    n = atoi(argv[1]);
    num_threads = atoi(argv[2]);


    // Aloca a matriz
    mat = (int**) malloc(sizeof(int*) * n);
    
    if(mat == NULL)
        return -1;

    for(int i = 0; i < n; i++) {
        mat[i] = (int*) malloc(sizeof(int) * n);
        if(mat[i] == NULL)
            return -1;

        for(int j = 0; j < n; j++) {
            mat[i][j] = 0;
        }
    }

    
    // Inicializa o lock de exclusão mútua
    pthread_mutex_init(&mutex, NULL);


    pthread_t tid[num_threads];
    int t, *id;


    auto start = chrono::high_resolution_clock::now();

    // Inicializa as threads
    for (t = 0; t < num_threads; t++) {
        if ((id = (int*) malloc(sizeof (int))) == NULL) {
            pthread_exit(NULL);
            return 1;
        }
        *id = t;

        //printf("--Cria a thread %d\n", t);
        if (pthread_create(&tid[t], NULL, ExecutaTarefa, (void *) id)) {
            //printf("--ERRO: pthread_create()\n");
            return -1;
        }
    }
    
    // Espera o término das threads
    for (t = 0; t < num_threads; t++) {
        if (pthread_join(tid[t], NULL)) {
            //printf("--ERRO: pthread_join() \n");
            return -1;
        }
    }
    
    
    chrono::duration<double> tempo = chrono::high_resolution_clock::now() - start;
    cout << "\nTempo de processamento: " << tempo.count();
    

    //imprimeMatriz();

    // Finaliza o lock de exclusão mútua
    pthread_mutex_destroy(&mutex);

    const int tamVet = (1 << (num_threads-1) ) + 1;
    long long qts[ tamVet ];
    //cout << "\n\n tamanho do vetor: " << ((1 << (num_threads-1) ) + 1);

    for(int i = 0; i < tamVet; i++) qts[i] = 0;

    long long qtOther, qtTotal = 0;
    qtOther = 0;

    for(int i = 0; i < n; i++) {
        for(int j = 0; j < n; j++) {
            if(mat[i][j] >= 0 & mat[i][j] < tamVet && mat[i][j] % 2 == 0 || mat[i][j] == 1) {
                qts[ mat[i][j] ]++;
            } else {
                qtOther++;
            }
            qtTotal++;
        }
    }

    //cout << "Tempo médio de processamento por célula: \n" << (double) tempo.count() / qtTotal;
    //cout << "Tempo de processamento total: " << tempo.count();

    cout << "\nDimensões da matriz: " << n << "x" << n << " (" << qtTotal << " células).";
    cout << "\nThreads: " << num_threads << "\n";


    for(int i = 0; i < log(tamVet-1)/log(2) + 1; i++) {
        cout <<  "\nQt " << i<< ": \t" << qts[(1 << i)] << "\t(" << (double) qts[(1 << i)] / qtTotal * 100 << "%)";
    }

    
    cout <<  "\n\nQt outros: " << qtOther << "\t(" << (double) qtOther / qtTotal * 100 << "%)";
    
    // Desaloca a matriz
    for(int i = 0; i < n; i++) {
        free(mat[i]);
    }

    free(mat);    

    cout << "\n\n";

    pthread_exit(NULL);
    return 0;
}
