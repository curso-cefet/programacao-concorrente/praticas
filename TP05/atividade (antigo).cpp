#include <iostream>
#include <pthread.h>
#include <chrono>

using namespace std;

#define NTHREADS  2

pthread_mutex_t mutex; 

int **mat;
int n;

void imprimeMatriz() {
    FILE *fsaida = fopen("saida-atividade", "w");


    if(fsaida == NULL) {
        perror ("Falha ao abrir arquivo de saída");
        return;
    } 


    for(int i = 0; i < n; i++) {
        for(int j = 0; j < n; j++) {
            fprintf(fsaida, "%d ", mat[i][j]);
        }


        fprintf(fsaida, "\n");
    }


    fclose(fsaida);

    cout << "\n\n";
}

void *ExecutaTarefa(void *threadid) {
    int *tid = (int*) threadid;

    *tid = *tid + 1;

    //printf("Thread : %d esta executando...\n", *tid);

    for(int i = 0; i < n; i++) {
        for(int j = 0; j < n; j++) {
            pthread_mutex_lock(&mutex); 

            //printf("Thread : %d na célula(%d, %d)...\n", *tid, i, j);
            if(mat[i][j] == 0) {
                mat[i][j] += *tid ;
            }
            
            pthread_mutex_unlock(&mutex);
        }
    }

    //printf("Thread : %d terminou!\n", *tid);
    free(threadid);
    pthread_exit(NULL);
    return 0;
}

// funcao/thread principal do programa
int main(int argc, char *argv[]) {

    if(argc < 2) {
        cout << "--ERRO: Argumentos inválidos\n\n";
        return -1;
    }

    // Leitura do argumento de tamanho da matriz
    n = atoi(argv[1]);


    // Aloca a matriz
    mat = (int**) malloc(sizeof(int*) * n);
    
    if(mat == NULL)
        return -1;

    for(int i = 0; i < n; i++) {
        mat[i] = (int*) malloc(sizeof(int) * n);
        if(mat[i] == NULL)
            return -1;

        for(int j = 0; j < n; j++) {
            mat[i][j] = 0;
        }
    }

    
    // Inicializa o lock de exclusão mútua
    pthread_mutex_init(&mutex, NULL);


    pthread_t tid[NTHREADS];
    int t, *id;


    auto start = chrono::high_resolution_clock::now();

    // Inicializa as threads
    for (t = 0; t < NTHREADS; t++) {
        if ((id = (int*) malloc(sizeof (int))) == NULL) {
            pthread_exit(NULL);
            return 1;
        }
        *id = t;

        //printf("--Cria a thread %d\n", t);
        if (pthread_create(&tid[t], NULL, ExecutaTarefa, (void *) id)) {
            //printf("--ERRO: pthread_create()\n");
            return -1;
        }
    }
    
    // Espera o término das threads
    for (t = 0; t < NTHREADS; t++) {
        if (pthread_join(tid[t], NULL)) {
            //printf("--ERRO: pthread_join() \n");
            return -1;
        }
    }
    
    
    chrono::duration<double> tempo = chrono::high_resolution_clock::now() - start;
    cout << "\nTempo de processamento: " << tempo.count() << "\n";
    

    //imprimeMatriz();

    // Finaliza o lock de exclusão mútua
    pthread_mutex_destroy(&mutex);


    int qt0, qt1, qt2, qtOther, qtTotal;
    qt0 = qt1 = qt2 = qtOther = 0;

    for(int i = 0; i < n; i++) {
        for(int j = 0; j < n; j++) {
            if(mat[i][j] == 0)
                qt0++;
            else if(mat[i][j] == 1)
                qt1++;
            else if(mat[i][j] == 2)
                qt2++;
            else
                qtOther++;
            
        }
    }

    qtTotal = qt0 + qt1 + qt2 + qtOther;

    cout << "Tempo médio de processamento por célula: " << (double) tempo.count() / qtTotal;

    cout << "\n\nDimensões da matriz: " << n << "x" << n << " (" << qtTotal << " células).\n";

    cout << "\nZeros: " << qt0 << "  \t(" << (double) qt0/qtTotal * 100 << "%)";
    cout << "\nUns: " << qt1 << "  \t(" << (double) qt1/qtTotal * 100 << "%)"; 
    cout << "\nDois: " << qt2 << "  \t(" << (double) qt2/qtTotal * 100 << "%)";
    cout << "\nOutros: " << qtOther << "  \t(" << (double) qtOther/qtTotal * 100 << "%)";
    
    cout << "\n\nThread vencedora: " << ((qt1 >= qt2) ? "Thread 1" : "Thread 2");

    // Desaloca a matriz
    for(int i = 0; i < n; i++) {
        free(mat[i]);
    }

    free(mat);

    

    cout << "\n\n";

    pthread_exit(NULL);
    return 0;
}
