#include <stdio.h>
#include <stdlib.h> 
#include <pthread.h>
#include <semaphore.h>

#define NTHREADS 4


pthread_mutex_t x_mutex;
pthread_cond_t x_cond;

//Quantidade das threads iniciais (1 e 2) que foram executadas
int count = 0;

void *t1 (void *threadid) {
  int *tid = (int*) threadid;
  printf("[T%d] olá, tudo bem?\n", *tid);
  
  pthread_mutex_lock(&x_mutex);
  
  // Incrementa a quantide de threads iniciais que foram executadas
  count++;

  if (count == 2) {
   // Libera a execução das duas últimas threads
    pthread_cond_broadcast(&x_cond);
  }
  pthread_mutex_unlock(&x_mutex); 

  pthread_exit(NULL);
}

void *t2 (void *threadid) {
  int *tid = (int*) threadid;
  printf("[T%d] hello!\n", *tid);
  
  pthread_mutex_lock(&x_mutex);

  // Incrementa a quantide de threads iniciais que foram executadas
  count++;

  if (count == 2) {
   // Libera a execução das duas últimas threads
    pthread_cond_broadcast(&x_cond);
  }
  pthread_mutex_unlock(&x_mutex); 

  pthread_exit(NULL);
}

void *t3 (void *threadid) {
  int *tid = (int*) threadid;

  pthread_mutex_lock(&x_mutex);
  
  if(count < 2) {
     // Espera pela execução das duas primeiras threads
     pthread_cond_wait(&x_cond, &x_mutex);
  }

  printf("[T%d] até mais tarde.\n", *tid);
  

  pthread_mutex_unlock(&x_mutex); 

  pthread_exit(NULL);
}

void *t4 (void *threadid) {
  int *tid = (int*) threadid;

  pthread_mutex_lock(&x_mutex);

  if(count < 2) {
     // Espera pela execução das duas primeiras threads
     pthread_cond_wait(&x_cond, &x_mutex);
  }

  printf("[T%d] tchau!\n", *tid);
  
  pthread_mutex_unlock(&x_mutex); 


  pthread_exit(NULL);
}

//Função principal
int main(int argc, char *argv[]) {
  pthread_t tid[NTHREADS];
  int *id[NTHREADS], t;


  for (t=0; t<NTHREADS; t++) {
    if ((id[t] = (int*) malloc(sizeof(int))) == NULL) {
       pthread_exit(NULL); return 1;
    }
    *id[t] = t+1;
  }

  /* Inicilaiza o mutex (lock de exclusao mutua) e a variável de condicao */
  pthread_mutex_init(&x_mutex, NULL);
  pthread_cond_init (&x_cond, NULL);
  

  //Inicializa as threads
  if (pthread_create(&tid[0], NULL, t1, (void *)id[0])) { printf("--ERRO: pthread_create()\n"); exit(-1); }
  if (pthread_create(&tid[1], NULL, t2, (void *)id[1])) { printf("--ERRO: pthread_create()\n"); exit(-1); }
  if (pthread_create(&tid[2], NULL, t3, (void *)id[2])) { printf("--ERRO: pthread_create()\n"); exit(-1); }
  if (pthread_create(&tid[3], NULL, t4, (void *)id[3])) { printf("--ERRO: pthread_create()\n"); exit(-1); }



  for (t=0; t<NTHREADS; t++) {
    if (pthread_join(tid[t], NULL)) {
         printf("--ERRO: pthread_join() \n"); exit(-1); 
    } 
  }

  /* Desaloca variaveis e termina */
  pthread_mutex_destroy(&x_mutex);
  pthread_cond_destroy(&x_cond);

  pthread_exit(NULL);
}
